CREATE TABLE league_user (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    user_password_id INTEGER NULL REFERENCES user_password(id)
);
