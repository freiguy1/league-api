use iron::prelude::*;
use iron::status;

use super::contract::UpdateLeagueRequest;
use league_db::repo::{
    ReferenceRepo, TeamEventRepo, UserRepo, UpdateLeague, LeagueRepo,
    LeagueDao, MatchupLocation, Administrator, NewMatchupLocation, NewAdministrator
};

use std::collections::HashSet;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a number")));
    };

    let mut update_league_request = match ::util::parse_body::<UpdateLeagueRequest>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = LeagueRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let user_repo = UserRepo::new(&conn);
    let team_events = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));
    let current_league = match itry!(repo.get_single(league_id)) {
        Some(l) => l,
        None => return Ok(Response::with(status::NotFound))
    };
    let sports = ::util::reference::sports(&context.cache, &reference_repo);
    let request_user_ids: HashSet<i32> = update_league_request.administrators.iter().map(|admin| admin.user_id).collect();
    let request_users = itry!(user_repo.get_many(&request_user_ids.iter().map(|id| *id).collect()));
    let used_location_ids: HashSet<i32> = team_events.into_iter()
        .filter_map(|te| te.matchup())
        .map(|g| g.matchup_location_id)
        .collect();

    let validation_errors = super::validator::validate(&mut update_league_request, &sports, &used_location_ids, Some(&current_league), &request_users);
    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }


    let dao = build_update_dao(league_id, update_league_request, sports, current_league);

    itry!(repo.update(&dao));

    Ok(Response::with(status::Ok))
}

fn build_update_dao(league_id: i32,
                    req: UpdateLeagueRequest,
                    sports: Vec<::util::reference::KeyValue>,
                    current_league: LeagueDao) -> UpdateLeague {
    let sport_id = sports
        .iter()
        .filter(|s| s.value.to_lowercase() == req.sport.to_lowercase())
        .next()
        .unwrap().key;

    // location shenanigans
    let request_location_ids: HashSet<i32> = req.matchup_locations.iter().filter_map(|loc| loc.id).collect();
    let current_location_ids: HashSet<i32> = current_league.matchup_locations.iter().map(|loc| loc.id).collect();
    let remove_matchup_locations: Vec<i32> = current_location_ids.difference(&request_location_ids).map(|&id| id).collect();
    let update_matchup_locations: Vec<MatchupLocation> = req.matchup_locations.iter()
        .filter(|loc| loc.id.is_some())
        .filter(|loc| {
            let current_loc = current_league.matchup_locations.iter().find(|gl| gl.id == loc.id.unwrap()).unwrap();
            loc.name != current_loc.name
        }).map(|loc| MatchupLocation {
            id: loc.id.unwrap(),
            name: loc.name.clone()
        }).collect();
    let new_matchup_locations: Vec<NewMatchupLocation> = req.matchup_locations.iter()
        .filter(|loc| loc.id.is_none())
        .map(|loc| NewMatchupLocation { name: loc.name.clone() }).collect();

    // admin shenanigans
    let request_admin_ids: HashSet<i32> = req.administrators.iter().filter_map(|ad| ad.id).collect();
    let current_admin_ids: HashSet<i32> = current_league.administrators.iter().map(|ad| ad.id).collect();
    let remove_administrators: Vec<i32> = current_admin_ids.difference(&request_admin_ids).map(|&id| id).collect();
    let update_administrators: Vec<Administrator> = req.administrators.iter()
        .filter(|ad| ad.id.is_some())
        .filter(|ad| {
            let current_ad = current_league.administrators.iter().find(|ad2| ad2.id == ad.id.unwrap()).unwrap();
            ad.user_id != current_ad.user_id
        }).map(|ad| Administrator {
            id: ad.id.unwrap(),
            user_id: ad.user_id
        }).collect();
    let new_administrators: Vec<NewAdministrator> = req.administrators.iter()
        .filter(|ad| ad.id.is_none())
        .map(|ad| NewAdministrator { user_id: ad.user_id }).collect();

    UpdateLeague {
        id: league_id,
        name: req.name,
        information: req.information,
        sport_id: sport_id,
        organization: req.organization.clone(),
        contact_phone: req.contact_phone.clone(),
        remove_matchup_locations: remove_matchup_locations,
        update_matchup_locations: update_matchup_locations,
        new_matchup_locations: new_matchup_locations,
        remove_administrators: remove_administrators,
        update_administrators: update_administrators,
        new_administrators: new_administrators,
        games_per_matchup: req.games_per_matchup as i32
    }
}
