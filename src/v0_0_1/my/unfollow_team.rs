use iron::prelude::*;
use iron::status;

use league_db::repo::{TeamRepo, TeamFollowerRepo};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_id = if let Some(team_id) = ::util::get_i32_from_request(&req, "team_id") {
        team_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_id in url should be a number")));
    };
    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let user_id = context.user_id.unwrap(); // This is guaranteed given the Around
    let ref conn = itry!(context.pool.get());
    let follower_repo = TeamFollowerRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);

    if itry!(team_repo.get_by_id(team_id)).is_none() {
        return Ok(Response::with(status::NotFound));
    }

    let mut followed_team_ids = itry!(follower_repo.get_teams_by_user_id(user_id))
        .into_iter()
        .map(|(t, _)| t.id);

    if !followed_team_ids.any(|tid| tid == team_id) {
        return Ok(Response::with(status::BadRequest));
    }

    itry!(follower_repo.delete(user_id, team_id));
    Ok(Response::with(status::Ok))
}

