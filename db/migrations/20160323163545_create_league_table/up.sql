CREATE TABLE league (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    information TEXT NULL,
    contact_email VARCHAR NOT NULL,
    contact_phone VARCHAR NOT NULL,
    sport_id INTEGER NOT NULL REFERENCES sport(id)
);
