use iron::prelude::*;
use iron::{AroundMiddleware, status};
use router::Router;
use chrono::{DateTime, FixedOffset, UTC};
use urlencoded::UrlEncodedQuery;

use league_db::repo::{LogRepo, LogDao};

use ::auth::rules::{RuleChecker, AppAdminRule};

#[derive(Serialize)]
struct LogEntry {
    id: i32,
    context: String,
    description: String,
    datetime: DateTime<UTC>,
    severity: i32,
    http_verb: Option<String>,
    request_url: Option<String>,
    authenticated_user_id: Option<i32>
}

pub fn init_routes(router: &mut Router) {
    let app_admin_rule = RuleChecker(AppAdminRule {});
    // Get all
    router.get("logs", app_admin_rule.around(Box::new(handle)), "get_logs");
}

fn handle(req: &mut Request) -> IronResult<Response> {
    let (early, late) = match req.get::<UrlEncodedQuery>() {
        Ok(hashmap) => {
            let early = hashmap.get("early")
                .and_then(|v| v.first())
                .and_then(|v| DateTime::<FixedOffset>::parse_from_rfc3339(&v.to_string()).ok())
                .map(|dt| dt.naive_utc());

            let late = hashmap.get("late")
                .and_then(|v| v.first())
                .and_then(|v| DateTime::<FixedOffset>::parse_from_rfc3339(&v.to_string()).ok())
                .map(|dt| dt.naive_utc());
            (early, late)
        },
        Err(_) => (None, None)
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = LogRepo::new(&conn);

    let log_daos = itry!(repo.get_in_span(&early, &late));
    let entries = build_response(log_daos);
    let response_body = itry!(::serde_json::to_string(&entries));
    Ok(::util::json_response(response_body, status::Ok))
}

fn build_response(daos: Vec<LogDao>) -> Vec<LogEntry> {
    daos.into_iter().map(|d| LogEntry {
        id: d.id,
        context: d.context,
        description: d.description,
        datetime: DateTime::from_utc(d.datetime, UTC),
        severity: d.severity,
        http_verb: d.http_verb,
        request_url: d.request_url,
        authenticated_user_id: d.authenticated_user_id
    }).collect()
}
