#[allow(dead_code)]
use diesel::pg::PgConnection;
use error::Error;
use std::collections::HashSet;

mod league;
mod league_administrator;
mod matchup_location;

pub struct LeagueDao {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport_id: i32,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: i32,
    pub matchup_locations: Vec<MatchupLocation>,
    pub administrators: Vec<Administrator>
}

impl LeagueDao {
    fn from_dao(league: league::LeagueDao,
                admins: Vec<league_administrator::LeagueAdministratorDao>,
                locs: Vec<matchup_location::MatchupLocationDao>) -> LeagueDao {
        LeagueDao {
            id: league.id,
            name: league.name,
            information: league.information,
            sport_id: league.sport_id,
            organization: league.organization,
            contact_phone: league.contact_phone,
            games_per_matchup: league.games_per_matchup,
            matchup_locations: locs.into_iter().map(|l| MatchupLocation {
                id: l.id,
                name: l.name
            }).collect(),
            administrators: admins.into_iter().map(|a| Administrator {
                id: a.id,
                user_id: a.user_id
            }).collect()
        }
    }

    fn from_daos(leagues: Vec<league::LeagueDao>,
                 admins: Vec<league_administrator::LeagueAdministratorDao>,
                 locs: Vec<matchup_location::MatchupLocationDao>) -> Vec<LeagueDao> {
        leagues.iter().map(|l| LeagueDao {
            id: l.id,
            name: l.name.clone(),
            information: l.information.clone(),
            sport_id: l.sport_id,
            organization: l.organization.clone(),
            contact_phone: l.contact_phone.clone(),
            games_per_matchup: l.games_per_matchup,
            matchup_locations: locs.iter().filter(|l2| l2.league_id == l.id).map(|l| MatchupLocation {
                id: l.id,
                name: l.name.clone()
            }).collect(),
            administrators: admins.iter().filter(|a| a.league_id == l.id).map(|a| Administrator {
                id: a.id,
                user_id: a.user_id
            }).collect()
        }).collect()
    }
}

pub struct Administrator {
    pub id: i32,
    pub user_id: i32
}

pub struct MatchupLocation {
    pub id: i32,
    pub name: String
}

pub struct UpdateLeague {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport_id: i32,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: i32,
    pub new_matchup_locations: Vec<NewMatchupLocation>,
    pub update_matchup_locations: Vec<MatchupLocation>,
    pub remove_matchup_locations: Vec<i32>,
    pub new_administrators: Vec<NewAdministrator>,
    pub update_administrators: Vec<Administrator>,
    pub remove_administrators: Vec<i32>
}

impl UpdateLeague {
    fn get_league_dao(&self) -> league::LeagueDao {
        league::LeagueDao {
            id: self.id,
            name: self.name.clone(),
            information: self.information.clone(),
            sport_id: self.sport_id,
            organization: self.organization.clone(),
            contact_phone: self.contact_phone.clone(),
            games_per_matchup: self.games_per_matchup
        }
    }

    fn get_create_admin_daos(&self) -> Vec<league_administrator::NewLeagueAdministrator> {
        self.new_administrators.iter().map(|a| league_administrator::NewLeagueAdministrator {
            league_id: self.id,
            user_id: a.user_id
        }).collect()
    }

    fn get_create_loc_daos(&self) -> Vec<matchup_location::NewMatchupLocation> {
        self.new_matchup_locations.iter().map(|l| matchup_location::NewMatchupLocation {
            league_id: self.id,
            name: l.name.clone()
        }).collect()
    }

    fn get_update_admin_daos(&self) -> Vec<league_administrator::LeagueAdministratorDao> {
        self.update_administrators.iter().map(|a| league_administrator::LeagueAdministratorDao {
            id: a.id,
            league_id: self.id,
            user_id: a.user_id
        }).collect()
    }

    fn get_update_loc_daos(&self) -> Vec<matchup_location::MatchupLocationDao> {
        self.update_matchup_locations.iter().map(|l| matchup_location::MatchupLocationDao {
            id: l.id,
            league_id: self.id,
            name: l.name.clone()
        }).collect()
    }
}

pub struct NewLeague {
    pub name: String,
    pub information: Option<String>,
    pub sport_id: i32,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: i32,
    pub matchup_locations: Vec<NewMatchupLocation>,
    pub administrators: Vec<NewAdministrator>
}

impl NewLeague {
    fn get_league_dao(&self) -> league::NewLeague {
        league::NewLeague {
            name: self.name.clone(),
            information: self.information.clone(),
            sport_id: self.sport_id,
            organization: self.organization.clone(),
            contact_phone: self.contact_phone.clone(),
            games_per_matchup: self.games_per_matchup
        }
    }

    fn get_admin_daos(&self, league_id: i32) -> Vec<league_administrator::NewLeagueAdministrator> {
        self.administrators.iter().map(|a| league_administrator::NewLeagueAdministrator {
            league_id: league_id,
            user_id: a.user_id
        }).collect()
    }

    fn get_loc_daos(&self, league_id: i32) -> Vec<matchup_location::NewMatchupLocation> {
        self.matchup_locations.iter().map(|l| matchup_location::NewMatchupLocation {
            league_id: league_id,
            name: l.name.clone()
        }).collect()
    }
}

pub struct NewAdministrator {
    pub user_id: i32
}

pub struct NewMatchupLocation {
    pub name: String
}

pub struct LeagueRepo<'a> {
    league_repo: league::LeagueRepo<'a>,
    loc_repo: matchup_location::MatchupLocationRepo<'a>,
    admin_repo: league_administrator::LeagueAdministratorRepo<'a>
}

impl <'a> LeagueRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> LeagueRepo {
        LeagueRepo {
            league_repo: league::LeagueRepo::new(conn),
            loc_repo: matchup_location::MatchupLocationRepo::new(conn),
            admin_repo: league_administrator::LeagueAdministratorRepo::new(conn)
        }
    }

    pub fn search(&self, query: &String) -> Result<Vec<LeagueDao>, Error> {
        let league_daos = try!(self.league_repo.search(query));
        let ids: Vec<i32> = league_daos.iter().map(|d| d.id).collect();
        let locs = try!(self.loc_repo.get_by_league_ids(&ids));
        let admins = try!(self.admin_repo.get_by_league_ids(&ids));
        Ok(LeagueDao::from_daos(league_daos, admins, locs))
    }

    pub fn get_single(&self, id: i32) -> Result<Option<LeagueDao>, Error> {
        let league_dao = match try!(self.league_repo.get_single(id)) {
            Some(l) => l,
            None => return Ok(None)
        };
        let admins = try!(self.admin_repo.get_by_league_id(id));
        let locs = try!(self.loc_repo.get_by_league_id(id));
        Ok(Some(LeagueDao::from_dao(league_dao, admins, locs)))
    }

    pub fn get_many(&self, ids: &HashSet<i32>) -> Result<Vec<LeagueDao>, Error> {
        if ids.len() == 0 { return Ok(Vec::new()); }
        let league_daos = try!(self.league_repo.get_many(ids));
        let ids: Vec<i32> = league_daos.iter().map(|d| d.id).collect();
        let locs = try!(self.loc_repo.get_by_league_ids(&ids));
        let admins = try!(self.admin_repo.get_by_league_ids(&ids));
        Ok(LeagueDao::from_daos(league_daos, admins, locs))
    }

    pub fn get_administrators_by_league_id(&self, league_id: i32) -> Result<Vec<Administrator>, Error> {
        let daos = try!(self.admin_repo.get_by_league_id(league_id));
        Ok(daos.into_iter().map(|d| Administrator {
            id: d.id,
            user_id: d.user_id
        }).collect())
    }

    pub fn create(&self, new_league: &NewLeague) -> Result<i32, Error> {
        let created_league = try!(self.league_repo.create(&new_league.get_league_dao()));
        let league_id = created_league.id;
        let admin_daos = new_league.get_admin_daos(league_id);
        try!(self.admin_repo.create_many(&admin_daos));
        let loc_daos = new_league.get_loc_daos(league_id);
        try!(self.loc_repo.create_many(&loc_daos));
        Ok(league_id)
    }

    pub fn update(&self, updated_league: &UpdateLeague) -> Result<(), Error> {
        let league_dao = updated_league.get_league_dao();
        try!(self.league_repo.update(&league_dao));
        
        // Admins
        try!(self.admin_repo.delete_many(&updated_league.remove_administrators));
        for admin in updated_league.get_update_admin_daos().iter() {
            try!(self.admin_repo.update(&admin));
        }
        let create_admin_daos = updated_league.get_create_admin_daos();
        try!(self.admin_repo.create_many(&create_admin_daos));

        // Locations
        try!(self.loc_repo.delete_many(&updated_league.remove_matchup_locations));
        for loc in updated_league.get_update_loc_daos().iter() {
            try!(self.loc_repo.update(&loc));
        }
        let create_loc_daos = updated_league.get_create_loc_daos();
        try!(self.loc_repo.create_many(&create_loc_daos));

        Ok(())
    }
    
    pub fn delete(&self, league_id: i32) -> Result<(), Error> {
        try!(self.loc_repo.delete_by_league_id(league_id));
        try!(self.admin_repo.delete_by_league_id(league_id));
        try!(self.league_repo.delete(league_id));
        Ok(())
    }

}
