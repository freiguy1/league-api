use iron::prelude::*;
use iron::status;
use std::collections::HashMap;

use super::contract::GetTeamsResponseItem;
use league_db::repo::{TeamRepo, TeamDao, LeagueRepo, TeamEventRepo, TeamFollowerRepo};
use stats::record::Record;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let cache = context.cache.clone();
    let ref conn = itry!(context.pool.get());
    let repo = TeamRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_follower_repo = TeamFollowerRepo::new(&conn);

    let league_dao = match itry!(league_repo.get_single(league_id)) {
        Some(ld) => ld,
        None => return Ok(Response::with(status::NotFound))
    };

    let daos = itry!(repo.get_by_league_id(league_id));
    let is_league_admin = context.user_id.map(|uid| league_dao.administrators.iter().any(|a| a.user_id == uid)).unwrap_or(false);

    // Get Records
    let team_ids = daos.iter().map(|dao| dao.id).collect();
    let records = itry!(::util::record::get_records_for_team_ids(team_ids, cache, &team_event_repo));

    // Get followed teams
    let followed_teams = match context.user_id {
        Some(uid) => itry!(team_follower_repo.get_teams_by_user_id(uid)).into_iter().map(|(t, _)| t.id).collect(),
        None => Vec::new()
    };

    let teams = build_response(daos, records, is_league_admin, followed_teams);

    let response_body = itry!(::serde_json::to_string(&teams));
    Ok(::util::json_response(response_body, status::Ok))
}

fn build_response(daos: Vec<TeamDao>, records: HashMap<i32, Record>, is_league_admin: bool, followed_teams: Vec<i32>) -> Vec<GetTeamsResponseItem> {
    let default_record = Record { win: 0, loss: 0, tie: 0 };
    daos
        .into_iter()
        .map(|team| {
            let record = records.get(&team.id).unwrap_or(&default_record);
            GetTeamsResponseItem {
                sequence: team.sequence,
                id: team.id,
                name: team.name.clone(),
                captain_name: if is_league_admin { Some(team.captain_name.clone()) } else { None },
                captain_email: if is_league_admin { Some(team.captain_email.clone()) } else { None },
                captain_phone: if is_league_admin { Some(team.captain_phone.clone()) } else { None },
                wins: record.win,
                losses: record.loss,
                ties: record.tie,
                is_followed: followed_teams.iter().any(|&tid| tid == team.id)
            }
        })
        .collect()
}
