use iron::prelude::*;
use iron::status;
use chrono::{DateTime, UTC};
use std::collections::HashMap;

use super::contract::{TeamEventSummary, GetTeamResponse, Game};
use league_db::repo::{TeamRepo, TeamDao, LeagueRepo, TeamEventRepo, TeamEvent, ReferenceRepo, LeagueDao, TeamFollowerRepo};
use stats::record::Record;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_id = if let Some(team_id) = ::util::get_i32_from_request(&req, "team_id") {
        team_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let cache = context.cache.clone();
    let repo = TeamRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let team_follower_repo = TeamFollowerRepo::new(&conn);

    let dao = match itry!(repo.get_by_id(team_id)) {
        Some(dao) => dao,
        None => return Ok(Response::with(status::NotFound))
    };

    let league_dao = match itry!(league_repo.get_single(dao.league_id)) {
        Some(ld) => ld,
        None => return Ok(Response::with(status::NotFound))
    };

    let team_daos = itry!(repo.get_by_league_ids(&(vec![dao.league_id].into_iter().collect())));
    let team_names: HashMap<i32, String> = team_daos.iter().map(|t| (t.id, t.name.clone())).collect();
    let is_league_admin = context.user_id.map(|uid| league_dao.administrators.iter().any(|a| a.user_id == uid)).unwrap_or(false);

    let mut team_events: Vec<TeamEvent> = itry!(team_event_repo.get_by_team_ids(&vec![dao.id].into_iter().collect()))
        .into_iter().collect();
    team_events.sort_by_key(|t| t.datetime().clone());
    let sports = ::util::reference::sports(&context.cache, &reference_repo);
    let sport = sports.into_iter().find(|s| s.key == league_dao.sport_id).unwrap().value;
    let mut records = itry!(::util::record::get_records_for_team_ids(vec![dao.id].into_iter().collect(), cache, &team_event_repo));
    let record = records.remove(&dao.id).unwrap_or(Record { win: 0, loss: 0, tie: 0 });
    let statuses = ::util::reference::matchup_statuses(&context.cache, &reference_repo);

    let follower_user_ids = itry!(team_follower_repo.get_users_by_team_id(dao.id)).iter().map(|u| u.0.id).collect::<Vec<_>>();

    let team = build_response(dao, league_dao, record, is_league_admin, team_events, sport, team_names, statuses, follower_user_ids, context.user_id);
    let response_body = itry!(::serde_json::to_string(&team));
    Ok(::util::json_response(response_body, status::Ok))
}


fn build_response(
    dao: TeamDao,
    league: LeagueDao,
    record: Record,
    is_league_admin: bool,
    team_events: Vec<TeamEvent>,
    sport: String,
    team_names: HashMap<i32, String>,
    statuses: Vec<::util::reference::KeyValue>,
    follower_user_ids: Vec<i32>,
    context_user_id: Option<i32>) -> GetTeamResponse {
    let future_team_events = team_events.iter().filter(|t| t.datetime() > &UTC::now().naive_utc()).collect::<Vec<_>>();
    let next_team_events = if let Some(next_team_event) = future_team_events.first() {
        let date = next_team_event.datetime().date();
        future_team_events.iter().filter(|te| te.datetime().date() == date)
            .map(|te| make_team_event_summary(te, dao.id, &league, &team_names, &statuses))
            .collect()
    } else { vec![] };

    GetTeamResponse {
        sequence: dao.sequence,
        id: dao.id,
        name: dao.name.clone(),
        league_id: league.id,
        league_name: league.name.clone(),
        league_organization: league.organization.clone(),
        captain_name: if is_league_admin { Some(dao.captain_name.clone()) } else { None },
        captain_email: if is_league_admin { Some(dao.captain_email.clone()) } else { None },
        captain_phone: if is_league_admin { Some(dao.captain_phone.clone()) } else { None },
        wins: record.win,
        losses: record.loss,
        ties: record.tie,
        remaining_team_events: future_team_events.len() as i32,
        sport: sport,
        next_team_events: next_team_events,
        previous_team_event: team_events.iter()
            .filter(|t| t.datetime() < &UTC::now().naive_utc())
            .last().map(|te| make_team_event_summary(
                te,
                dao.id,
                &league,
                &team_names,
                &statuses)),
        is_followed: context_user_id.map(|uid| follower_user_ids.contains(&uid)).unwrap_or(false),
        num_followers: follower_user_ids.len() as i32
    }
}

fn make_team_event_summary(te: &TeamEvent,
                     team_id: i32,
                     league: &LeagueDao,
                     team_names: &HashMap<i32, String>,
                     statuses: &Vec<::util::reference::KeyValue>) -> TeamEventSummary {
    match te {
        &TeamEvent::Matchup(ref m) => {
            TeamEventSummary {
                id: m.id,
                notes: m.notes.clone(),
                datetime: Some(DateTime::from_utc(m.datetime, UTC)),
                location: Some(league.matchup_locations.iter().find(|gl| gl.id == m.matchup_location_id).unwrap().name.clone()),
                is_home: Some(team_id == m.home_team_id),
                against_team_name: Some(
                    if team_id == m.home_team_id { team_names[&m.away_team_id].clone() }
                    else { team_names[&m.home_team_id].clone() }),
                bye_date: None,
                status: Some(statuses.iter().filter(|s| s.key == m.matchup_status_id).next().unwrap().value.clone()),
                games: Some(m.games.iter().map(|g| Game {
                    id: g.id,
                    home_team_score: g.home_team_score,
                    away_team_score: g.away_team_score,
                    sequence: g.sequence,
                    notes: g.notes.clone()
                }).collect())
            }
        },
        &TeamEvent::Bye(ref b) => TeamEventSummary {
            id: b.id,
            notes: b.notes.clone(),
            datetime: None,
            location: None,
            is_home: None,
            against_team_name: None,
            bye_date: Some(b.datetime.date()),
            status: None,
            games: None
        }
    }
}
