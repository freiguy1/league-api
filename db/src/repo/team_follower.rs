use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::{team_follower, league_user, team};
use diesel::expression::dsl::*;
use error::Error;

#[derive(Queryable, AsChangeset)]
#[table_name="team_follower"]
pub struct TeamFollowerDao {
    pub id: i32,
    pub team_id: i32,
    pub user_id: i32
}

#[derive(Insertable)]
#[table_name="team_follower"]
pub struct NewTeamFollower {
    pub team_id: i32,
    pub user_id: i32
}


pub struct TeamFollowerRepo<'a> {
    conn: &'a PgConnection
}

impl <'a> TeamFollowerRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> TeamFollowerRepo {
        TeamFollowerRepo {
            conn: conn
        }
    }

    pub fn create(&self, new_team_follower: &NewTeamFollower) -> Result<TeamFollowerDao, Error> {
        let result = try!(::diesel::insert(new_team_follower).into(team_follower::table)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete(&self, user_id: i32, team_id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(team_follower::table
            .filter(team_follower::user_id.eq(user_id))
            .filter(team_follower::team_id.eq(team_id)))
            .execute(self.conn));
        Ok(result)
    }

    pub fn delete_by_team_id(&self, team_id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(team_follower::table
            .filter(team_follower::team_id.eq(team_id)))
            .execute(self.conn));
        Ok(result)
    }

    pub fn get_follower_count_by_team_id(&self, team_id: i32) -> Result<i32, Error> {
        let count = try!(team_follower::table
            .filter(team_follower::team_id.eq(team_id))
            .select(team_follower::id)
            .first(self.conn));
        Ok(count)
    }

    pub fn get_users_by_team_id(&self, team_id: i32) -> Result<Vec<(::repo::UserDao, TeamFollowerDao)>, Error> {
        let followers: Vec<TeamFollowerDao> = try!(team_follower::table
            .filter(team_follower::team_id.eq(team_id))
            .order(team_follower::user_id)
            .load(self.conn));

        let user_ids: Vec<i32> = followers.iter().map(|f| f.user_id).collect();

        let users: Vec<::repo::UserDao> = try!(league_user::table
            .filter(league_user::id.eq(any(user_ids)))
            .order(league_user::id)
            .load(self.conn));

        Ok(users.into_iter().zip(followers).collect())
    }

    pub fn get_teams_by_user_id(&self, user_id: i32) -> Result<Vec<(::repo::TeamDao, TeamFollowerDao)>, Error> {
        let followers: Vec<TeamFollowerDao> = try!(team_follower::table
            .filter(team_follower::user_id.eq(user_id))
            .order(team_follower::team_id)
            .load(self.conn));

        let team_ids: Vec<i32> = followers.iter().map(|f| f.team_id).collect();

        let teams: Vec<::repo::TeamDao> = try!(team::table
            .filter(team::id.eq(any(team_ids)))
            .order(team::id)
            .load(self.conn));
        Ok(teams.into_iter().zip(followers).collect())
    }
}


#[test]
fn follower_test() {
    // Flexible to work on CI build or localhost
    let connection_string = ::std::env::var("DATABASE_URL")
        .unwrap_or("postgres://localhost/league_dev?user=league_dev_user&password=league_dev_user_password".to_string());
    let conn = PgConnection::establish(&connection_string).expect("Couldn't unwrap connection");
    let follower_repo = TeamFollowerRepo::new(&conn);
    let team_repo = ::repo::TeamRepo::new(&conn);
    let user_repo = ::repo::UserRepo::new(&conn);
    let league_repo = ::repo::LeagueRepo::new(&conn);

    let new_league = ::repo::NewLeague {
        name: "Test League".to_string(),
        information: None,
        sport_id: 1,
        organization: "Test Organization".to_string(),
        contact_phone: "1234567890".to_string(),
        administrators: Vec::new(),
        matchup_locations: Vec::new(),
        games_per_matchup: 1
    };

    let league_id = league_repo.create(&new_league).expect("Couldn't create new league");

    let new_team = ::repo::NewTeam {
        sequence: 1,
        league_id: league_id,
        name: "Test team name".to_string(),
        captain_name: "Tester Captain".to_string(),
        captain_email: "test.captain@mail.com".to_string(),
        captain_phone: "1234567890".to_string()
    };

    let team_id = team_repo.create(&new_team).expect("Couldn't create new team").id;

    let new_user = ::repo::NewUser {
        first_name: "TestFirst".to_string(),
        last_name: "TestLast".to_string(),
        email: "test.user@mail.com".to_string(),
        phone: "1234567890".to_string(),
        user_password_id: None,
        confirmation_key: "abcdefghijklmnop".to_string(),
        created_datetime: ::chrono::UTC::now().naive_utc(),
        is_confirmed: true
    };

    let user_id = user_repo.create(&new_user).expect("Couldn't create new user").id;

    let new_follower = NewTeamFollower {
        team_id: team_id,
        user_id: user_id
    };
    follower_repo.create(&new_follower).expect("Couldn't create follower");

    let followers = follower_repo.get_users_by_team_id(team_id).expect("Couldn't get followers by team_id");
    assert!(followers.len() == 1);
    assert_eq!(followers[0].0.first_name, new_user.first_name);

    let followed_teams = follower_repo.get_teams_by_user_id(user_id).expect("Couldn't get teams by user_id");
    assert!(followed_teams.len() == 1);
    assert_eq!(followed_teams[0].0.name, new_team.name);

    follower_repo.delete(user_id, team_id).expect("Couldn't delete follower");
    user_repo.delete(user_id).expect("Couldn't delete user");
    team_repo.delete(team_id).expect("Couldn't delete team");
    league_repo.delete(league_id).expect("Couldn't delete league");
}
