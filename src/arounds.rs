use iron::prelude::*;
use iron::{AfterMiddleware, AroundMiddleware, Handler};
use hyper::header::{CacheControl, CacheDirective};


pub struct ErrorPrint;

impl AfterMiddleware for ErrorPrint {
    fn catch(&self, req: &mut Request, err: IronError) -> IronResult<Response> {
        let logger = req.extensions.get::<::context::ApiContext>().unwrap().logger();
        logger.log_with_request_error(req, &err);
        Err(err)
    }
}

pub struct HttpCacheHeader;

impl AroundMiddleware for HttpCacheHeader {
    fn around(self, handler: Box<Handler>) -> Box<Handler> {
        Box::new(HttpCacheHandler { handler: handler }) as Box<Handler>
    }
}

#[derive(Clone, Copy)]
struct HttpCacheHandler<H: Handler> {
    handler: H
}

impl <H:Handler> Handler for HttpCacheHandler<H> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        match self.handler.handle(req) {
            Ok(mut resp) => {
                resp.headers.set(CacheControl(vec![CacheDirective::MaxAge(600u32)]));
                Ok(resp)
            },
            Err(e) => Err(e)
        }
    }
}
