CREATE TABLE game (
    id SERIAL PRIMARY KEY,
    sequence INTEGER NOT NULL,
    team_event_id INTEGER NOT NULL REFERENCES matchup(team_event_id),
    home_team_score INTEGER NULL,
    away_team_score INTEGER NULL,
    notes TEXT NULL
);

ALTER TABLE matchup DROP COLUMN IF EXISTS home_team_score;
ALTER TABLE matchup DROP COLUMN IF EXISTS away_team_score;

