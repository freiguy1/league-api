CREATE TABLE game_location (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    league_id INTEGER NOT NULL REFERENCES league(id)
);
