use context::ApiContext;
use iron::prelude::*;
use iron::status;

use league_db::repo::UserRepo;


pub fn handle(req: &mut Request) -> IronResult<Response> {
    let req_confirm_key = match ::util::get_string_from_request(&req, "confirmation_key") {
        Some(key) => key,
        None => return Ok(Response::with((status::BadRequest, "invalid key")))
    };

    let context = req.extensions.get::<ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let user_repo = UserRepo::new(&conn);

    let mut user = match itry!(user_repo.get_by_confirmation_key(&req_confirm_key)) {
        Some(u) => u,
        None => return Ok(Response::with((status::BadRequest, "invalid key")))
    };

    if !user.is_confirmed {
        user.is_confirmed = true;
        itry!(user_repo.update(&user));
        Ok(Response::with((status::Ok, "user confirmed")))
    } else {
        Ok(Response::with(status::NotFound))
    }
}
