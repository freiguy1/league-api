use iron::AroundMiddleware;
use ::auth::rules::{RuleChecker, LeagueAdminRule, ConfirmedUserRule, LeagueAdminRuleParamType};
use router::Router;

pub mod contract;

mod create;
mod get_by_id;
mod update;
mod search;
mod validator;

pub fn init_routes(router: &mut Router) {
    let league_admin_rule = RuleChecker(LeagueAdminRule {
        param_name: "league_id" ,
        param_type: LeagueAdminRuleParamType::LeagueId
    });

    let confirmed_user_rule = RuleChecker(ConfirmedUserRule{});
    // Search
    let handler = Box::new(search::handle);
    let handler = ::arounds::HttpCacheHeader{}.around(handler);
    router.get("leagues/search", handler, "search_league");
    // Create
    router.post("leagues", confirmed_user_rule.around(Box::new(create::handle)), "create_league");
    // Get single
    let handler = Box::new(get_by_id::handle);
    let handler = ::arounds::HttpCacheHeader{}.around(handler);
    router.get("leagues/:league_id", handler, "get_league");
    // Update
    router.put("/leagues/:league_id", league_admin_rule.around(Box::new(update::handle)), "update_league");
}
