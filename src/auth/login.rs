use context::ApiContext;
use iron::prelude::*;
use iron::status;
use pwhash::bcrypt;
use hyper::header::AccessControlAllowOrigin;

use league_db::repo::{UserRepo, PasswordRepo};

use super::contracts::{LoginRequest, LoginResponse};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    // Get json request
    let parsed = req.get::<::bodyparser::Struct<LoginRequest>>();
    let login_request = match parsed {
        Ok(Some(success)) => success,
        Ok(None) => return Ok(Response::with((status::BadRequest, "Endpoint requires json body"))),
        Err(err) => return Ok(Response::with((status::BadRequest, format!("{:?}", err))))
    };

    let context = req.extensions.get::<ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let user_repo = UserRepo::new(&conn);
    let password_repo = PasswordRepo::new(&conn);

    let user_from_db = match itry!(user_repo.get_by_email(&login_request.email.to_lowercase())) {
        Some(user) => user,
        None => return Ok(Response::with((status::BadRequest, "Incorrect email or password")))
    };

    let password_id = match user_from_db.user_password_id {
        Some(id) => id,
        None => return Ok(Response::with((status::BadRequest, "Incorrect email or password")))
    };

    let password_from_db = itry!(password_repo.get_single(password_id)).unwrap();

    let pw_attempt_and_salt = format!("{}{}", login_request.password, password_from_db.salt);

    if bcrypt::verify(&pw_attempt_and_salt, &password_from_db.password) {
        let token = super::create_token(
            user_from_db.id,
            &context.config);

        let login_response = LoginResponse {
            token: token.clone()
        };
        let response_body = itry!(::serde_json::to_string(&login_response));
        let mut response = ::util::json_response(response_body, status::Ok);
        super::add_token_to_response(&mut response, token);
        response.headers.set(AccessControlAllowOrigin::Any);
        return Ok(response);
    } else {
        return Ok(Response::with((status::BadRequest, "Incorrect email or password")));
    }
}
