use league_db::repo::TeamEventRepo;
use stats::record::{calculate_records, Record};
use error::Error;
use std::collections::{HashSet, HashMap};
use std::sync::{Arc, Mutex};

const TEAM_RECORD_KEY_PREFIX: &'static str = "record_team_id_";

fn format_team_key(team_id: i32) -> String {
    format!("{}{}", TEAM_RECORD_KEY_PREFIX, team_id.to_string())
}

pub fn get_records_for_team_ids(
    team_ids: HashSet<i32>,
    cache: Arc<Mutex<::util::Cache>>,
    team_event_repo: &TeamEventRepo) -> Result<HashMap<i32, Record>, Error> {
    let mut cache = cache.lock().unwrap();
    let mut records: HashMap<i32, Record> = team_ids
        .iter()
        .filter_map(|id| cache.get::<Record>(format_team_key(*id))
            .map(|r| (*id, r)))
        .collect();
    let db_team_ids = team_ids
        .iter()
        .filter(|&id| !records.iter().any(|cr| cr.0 == id))
        .map(|id| *id)
        .collect::<HashSet<i32>>();

    if db_team_ids.len() == 0 {
        return Ok(records);
    }

    let team_event_daos = try!(team_event_repo.get_by_team_ids(&db_team_ids));
    let matchups = team_event_daos.iter().map(|dao| ::stats::Matchup::from_dao(dao)).filter_map(|g| g).collect();
    let mut calculated_records = calculate_records(&matchups, &db_team_ids);

    for team_id in db_team_ids.iter() {
        if !calculated_records.contains_key(team_id) {
            calculated_records.insert(*team_id, Record { win: 0, loss: 0, tie: 0 });
        }
    }

    for (team_id, record) in calculated_records.iter() {
        let key = format_team_key(*team_id);
        cache.insert(key, record);
    }

    records.extend(calculated_records.into_iter());

    Ok(records)
}

pub fn invalidate_records_for_team_id(team_id: i32, cache: Arc<Mutex<::util::Cache>>) {
    let key = format_team_key(team_id);
    let mut cache = cache.lock().unwrap();
    cache.invalidate(key);
}
