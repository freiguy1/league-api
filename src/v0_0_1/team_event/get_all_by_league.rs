use iron::prelude::*;
use iron::status;

use super::contract::{Game, GetTeamEventsResponseItem};
use league_db::repo::{TeamEventRepo, TeamEvent, Matchup, Bye, LeagueRepo, ReferenceRepo, MatchupLocation, TeamRepo};
use chrono::{DateTime, UTC};
use std::collections::HashMap;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let statuses = ::util::reference::matchup_statuses(&context.cache, &reference_repo);

    let league_dao = match itry!(league_repo.get_single(league_id)) {
        Some(l) => l,
        None => return Ok(Response::with(status::NotFound))
    };

    let team_names: HashMap<i32, String> = itry!(team_repo.get_by_league_id(league_id)).into_iter()
        .map(|t| (t.id, t.name)).collect();

    let daos = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));

    let team_events = build_response(daos, statuses, league_dao.matchup_locations, team_names);

    let response_body = itry!(::serde_json::to_string(&team_events));
    Ok(::util::json_response(response_body, status::Ok))
}

fn build_response(
    daos: Vec<TeamEvent>,
    statuses: Vec<::util::reference::KeyValue>,
    locations: Vec<MatchupLocation>,
    team_names: HashMap<i32, String>) -> Vec<GetTeamEventsResponseItem> {
    daos
        .into_iter()
        .map(|te| match te {
            TeamEvent::Matchup(m) => build_response_from_matchup(m, &statuses, &locations, &team_names),
            TeamEvent::Bye(b) => build_response_from_bye(b, &team_names)
        }).collect()
}

fn build_response_from_matchup(
    m: Matchup,
    statuses: &Vec<::util::reference::KeyValue>,
    locations: &Vec<MatchupLocation>,
    team_names: &HashMap<i32, String>) -> GetTeamEventsResponseItem {

    let games = m.games.iter().map(|g| Game {
        id: g.id,
        sequence: g.sequence,
        home_team_score: g.home_team_score,
        away_team_score: g.away_team_score,
        notes: g.notes.clone()
    }).collect();

    GetTeamEventsResponseItem {
        id: m.id,
        notes: m.notes.clone(),
        datetime: Some(DateTime::from_utc(m.datetime, UTC)),
        location_id: Some(m.matchup_location_id),
        location: Some(locations.iter().find(|l| l.id == m.matchup_location_id).unwrap().name.clone()),
        status: Some(statuses.iter().filter(|s| s.key == m.matchup_status_id).next().unwrap().value.clone()),
        rescheduled_team_event_id: m.rescheduled_team_event_id,
        home_team_id: Some(m.home_team_id),
        home_team_name: Some(team_names[&m.home_team_id].clone()),
        away_team_id: Some(m.away_team_id),
        away_team_name: Some(team_names[&m.away_team_id].clone()),
        bye_date: None,
        bye_team_id: None,
        bye_team_name: None,
        games: Some(games)
    }

}

fn build_response_from_bye(b: Bye, team_names: &HashMap<i32, String>) -> GetTeamEventsResponseItem {
    GetTeamEventsResponseItem {
        id: b.id,
        notes: b.notes.clone(),
        datetime: None,
        location_id: None,
        location: None,
        status: None,
        rescheduled_team_event_id: None,
        home_team_id: None,
        home_team_name: None,
        away_team_id: None,
        away_team_name: None,
        bye_date: Some(b.datetime.date()),
        bye_team_id: Some(b.team_id),
        bye_team_name: Some(team_names[&b.team_id].clone()),
        games: None
    }
}
