use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::league_user;
use error::Error;
use chrono::NaiveDateTime;

#[derive(Queryable, AsChangeset)]
#[table_name="league_user"]
#[changeset_options(treat_none_as_null = "true")]
pub struct UserDao {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub phone: String,
    pub user_password_id: Option<i32>,
    pub is_confirmed: bool,
    pub confirmation_key: String,
    pub created_datetime: NaiveDateTime
}

#[derive(Insertable)]
#[table_name="league_user"]
pub struct NewUser {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub phone: String,
    pub user_password_id: Option<i32>,
    pub is_confirmed: bool,
    pub confirmation_key: String,
    pub created_datetime: NaiveDateTime
}

pub struct UserRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> UserRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> UserRepo {
        UserRepo {
            conn: conn
        }
    }

    pub fn create(&self, new_user: &NewUser) -> Result<UserDao, Error> {
        let result = try!(::diesel::insert(new_user).into(league_user::table)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn get_single(&self, id: i32) -> Result<Option<UserDao>, Error> {
        let result = try!(league_user::table.filter(league_user::id.eq(id))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn get_many(&self, ids: &Vec<i32>) -> Result<Vec<UserDao>, Error> {
        if ids.len() == 0 { return Ok(Vec::new()); }
        let result = try!(league_user::table.filter(league_user::id.eq_any(ids)).load(self.conn));
        Ok(result)
    }

    pub fn get_by_email(&self, email: &String) -> Result<Option<UserDao>, Error> {
        let email = email.to_lowercase();
        let result = try!(league_user::table.filter(league_user::email.eq(email))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn get_by_confirmation_key(&self, confirmation_key: &String) -> Result<Option<UserDao>, Error> {
        let result = try!(league_user::table.filter(league_user::confirmation_key.eq(confirmation_key))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn update(&self, updated_user: &UserDao) -> Result<UserDao, Error> {
        let result = try!(::diesel::update(league_user::table.filter(league_user::id.eq(updated_user.id)))
            .set(updated_user)
            .get_result(self.conn));
        Ok(result)
    }
    
    pub fn delete(&self, id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(league_user::table.filter(league_user::id.eq(id)))
            .execute(self.conn));
        Ok(result)
    }
}
