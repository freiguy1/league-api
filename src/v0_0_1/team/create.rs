use iron::prelude::*;
use iron::status;

use super::contract::{CreateTeamRequest, CreateTeamResponse};
use league_db::repo::{NewTeam, TeamRepo, LeagueRepo, TeamDao};

use std::collections::HashSet;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a u32")));
    };

    let mut create_team_request = match ::util::parse_body::<CreateTeamRequest>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let validation_errors = super::validator::validate(&mut create_team_request);
    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = TeamRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);

    if itry!(league_repo.get_single(league_id)).is_none() {
        return Ok(Response::with(status::NotFound));
    }

    let existing_teams = itry!(repo.get_by_league_id(league_id));

    let team_sequence = get_team_sequence(&existing_teams);

    let dao = build_dao(league_id, team_sequence, create_team_request);

    let created_dao = itry!(repo.create(&dao));

    let create_team_response = CreateTeamResponse {
        sequence: team_sequence,
        id: created_dao.id
    };

    let response_body = itry!(::serde_json::to_string(&create_team_response));
    let response = ::util::json_response(response_body, status::Ok);
    Ok(response)
}

fn get_team_sequence(existing_teams: &Vec<TeamDao>) -> i32 {
    if existing_teams.len() == 0 {
        return 1;
    }
    let mut team_sequence: Vec<i32> = existing_teams.iter().map(|team| team.sequence).collect();
    team_sequence.sort();
    if team_sequence.len() == team_sequence[team_sequence.len() - 1] as usize {
        (team_sequence.len() + 1) as i32
    } else {
        let team_sequence: HashSet<i32> = team_sequence.iter().map(|&id| id).collect();
        let range_set: HashSet<i32> = (1..(team_sequence.len() + 1)).map(|num| num as i32).collect();
        *range_set.difference(&team_sequence).next().unwrap()
    }
}

fn build_dao(league_id: i32, team_sequence: i32, req: CreateTeamRequest) -> NewTeam {
    NewTeam {
        sequence: team_sequence,
        league_id: league_id,
        name: req.name,
        captain_name: req.captain_name,
        captain_email: req.captain_email,
        captain_phone: req.captain_phone
    }
}


