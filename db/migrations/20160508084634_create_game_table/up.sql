CREATE TABLE game (
    id SERIAL PRIMARY KEY,
    home_team_score INTEGER NULL,
    away_team_score INTEGER NULL,
    datetime TIMESTAMP NOT NULL,
    game_location_id INTEGER NOT NULL REFERENCES game_location(id),
    league_id INTEGER NOT NULL REFERENCES league(id),
    home_team_id INTEGER NOT NULL REFERENCES team(id),
    away_team_id INTEGER NOT NULL REFERENCES team(id),
    game_status_id INTEGER NOT NULL REFERENCES game_status(id),
    rescheduled_game_id INTEGER NULL REFERENCES game(id),
    notes TEXT NULL
);
