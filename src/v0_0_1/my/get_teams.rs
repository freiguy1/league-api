use chrono::{DateTime, UTC};
use iron::prelude::*;
use iron::status;
use std::collections::{HashSet, HashMap};

use super::contract::{GetTeamsResponseItem, NextTeamEvent};
use league_db::repo::{TeamFollowerRepo, TeamDao, TeamEventRepo, LeagueRepo, LeagueDao, TeamEvent, ReferenceRepo, TeamRepo};
use stats::record::Record;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let cache = context.cache.clone();
    let ref conn = itry!(context.pool.get());
    let follower_repo = TeamFollowerRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);

    let sports = ::util::reference::sports(&context.cache, &reference_repo);
    let user_id = context.user_id.unwrap(); // This is guaranteed given the Around
    let daos = itry!(follower_repo.get_teams_by_user_id(user_id));
    let daos: Vec<TeamDao> = daos.into_iter().map(|(t, _)| t).collect();
    let team_ids = daos.iter().map(|dao| dao.id).collect();
    let league_ids: HashSet<i32> = daos.iter().map(|dao| dao.league_id).collect();
    let league_daos: HashMap<i32, LeagueDao> = itry!(league_repo.get_many(&league_ids))
        .into_iter().map(|dao| (dao.id, dao)).collect();
    let mut team_events: Vec<TeamEvent> = itry!(team_event_repo.get_by_team_ids(&team_ids))
        .into_iter().filter(|t| t.datetime() > &UTC::now().naive_utc()).collect();
    team_events.sort_by_key(|t| t.datetime().clone());
    let records = itry!(::util::record::get_records_for_team_ids(team_ids, cache, &team_event_repo));
    let team_names: HashMap<i32, String> = itry!(team_repo.get_by_league_ids(&league_ids))
        .into_iter().map(|t| (t.id, t.name)).collect();
    let teams = build_response(daos, records, league_daos, team_events, sports, team_names);
    let response_body = itry!(::serde_json::to_string(&teams));
    Ok(::util::json_response(response_body, status::Ok))
}

fn build_response(daos: Vec<TeamDao>,
                  records: HashMap<i32, Record>,
                  leagues: HashMap<i32, LeagueDao>,
                  team_events: Vec<TeamEvent>,
                  sports: Vec<::util::reference::KeyValue>,
                  team_names: HashMap<i32, String>) -> Vec<GetTeamsResponseItem> {
    let default_record = Record { win: 0, loss: 0, tie: 0 };
    daos
        .into_iter()
        .map(|team| {
            let ref league = leagues[&team.league_id];
            let record = records.get(&team.id).unwrap_or(&default_record);
            let team_events = team_events.iter().filter(|t| t.team_ids().iter().any(|tid| tid == &team.id)).collect::<Vec<_>>();
            let next_team_events = if let Some(next_team_event) = team_events.first() {
                let date = next_team_event.datetime().date();
                team_events.iter().filter(|te| te.datetime().date() == date)
                    .map(|te| make_next_team_event(te, team.id, &league, &team_names))
                    .collect()
            } else { vec![] };
            let sport = sports.iter().filter(|s| s.key == league.sport_id).next().unwrap().value.clone();

            GetTeamsResponseItem {
                sequence: team.sequence,
                id: team.id,
                name: team.name.clone(),
                league_id: team.league_id,
                league_name: league.name.clone(),
                sport: sport,
                wins: record.win,
                losses: record.loss,
                ties: record.tie,
                remaining_team_events: team_events.len() as i32,
                next_team_events: next_team_events
            }
        })
        .collect()
}

fn make_next_team_event(te: &TeamEvent,
                  team_id: i32,
                  league: &LeagueDao,
                  team_name: &HashMap<i32, String>) -> NextTeamEvent {
    match te {
        &TeamEvent::Matchup(ref m) => {
            NextTeamEvent {
                id: m.id,
                notes: m.notes.clone(),
                datetime: Some(DateTime::from_utc(m.datetime, UTC)),
                location: Some(league.matchup_locations.iter().find(|gl| gl.id == m.matchup_location_id).unwrap().name.clone()),
                is_home: Some(team_id == m.home_team_id),
                against_team_name: Some(
                    if team_id == m.home_team_id { team_name[&m.away_team_id].clone() }
                    else { team_name[&m.home_team_id].clone() }),
                bye_date: None
            }
        },
        &TeamEvent::Bye(ref b) => NextTeamEvent {
            id: b.id,
            notes: b.notes.clone(),
            datetime: None,
            location: None,
            is_home: None,
            against_team_name: None,
            bye_date: Some(b.datetime.date())
        }
    }
}
