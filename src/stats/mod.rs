use league_db::repo::TeamEvent;

pub mod record;

const HOME_TEAM_WIN_ID: i32 = 1;
const AWAY_TEAM_WIN_ID: i32 = 2;
const HOME_TEAM_FORFEIT_ID: i32 = 3;
const AWAY_TEAM_FORFEIT_ID: i32 = 4;
const TIE_ID: i32 = 8;

#[derive(Debug)]
pub struct Matchup {
    pub home_team_id: i32,
    pub away_team_id: i32,
    pub matchup_status_id: i32
}

impl Matchup {
    pub fn from_dao(dao: &TeamEvent) -> Option<Matchup> {
        match *dao {
            TeamEvent::Matchup(ref m) => Some(Matchup {
                home_team_id: m.home_team_id,
                away_team_id: m.away_team_id,
                matchup_status_id: m.matchup_status_id
            }),
            _ => None
        }
    }

    fn is_home_team_win(&self) -> bool {
        self.matchup_status_id == HOME_TEAM_WIN_ID || self.matchup_status_id == AWAY_TEAM_FORFEIT_ID
    }

    fn is_away_team_win(&self) -> bool {
        self.matchup_status_id == AWAY_TEAM_WIN_ID || self.matchup_status_id == HOME_TEAM_FORFEIT_ID
    }

    fn is_tie(&self) -> bool {
        self.matchup_status_id == TIE_ID
    }

    fn is_complete(&self) -> bool {
        self.matchup_status_id == HOME_TEAM_WIN_ID ||
            self.matchup_status_id == AWAY_TEAM_WIN_ID ||
            self.matchup_status_id == HOME_TEAM_FORFEIT_ID ||
            self.matchup_status_id == AWAY_TEAM_FORFEIT_ID ||
            self.matchup_status_id == TIE_ID
    }
}
