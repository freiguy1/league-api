CREATE TABLE sport (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

INSERT INTO sport (name) values 
    ('Volleyball'), 
    ('Softball'), 
    ('Baseball'), 
    ('Basketball'),
    ('Football'), 
    ('Soccer'),
    ('Flag Football'),
    ('Darts'),
    ('Bowling'),
    ('Pool'),
    ('Tennis'),
    ('Badminton');
