use chrono::NaiveTime;
use iron::prelude::*;
use iron::status;
use std::collections::HashSet;

use super::contract::UpdateTeamEventRequest;
use league_db::repo::{
    TeamEventRepo, TeamEvent, ReferenceRepo, TeamRepo, LeagueRepo,
    UpdateMatchup, Bye, Game, NewGame
};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_event_id = if let Some(team_event_id) = ::util::get_i32_from_request(&req, "team_event_id") {
        team_event_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_event_id in url should be a u32")));
    };

    let update_team_event_request = match ::util::parse_body::<UpdateTeamEventRequest>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);

    let existing_team_event = match itry!(team_event_repo.get_single(team_event_id)) {
        Some(te) => te,
        None => return Ok(Response::with(status::NotFound))
    };
    let league_id = existing_team_event.league_id();

    let league = match itry!(league_repo.get_single(league_id)) {
        Some(l) => l,
        None => return Ok(Response::with(status::NotFound))
    };

    let existing_team_events = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));

    let statuses = ::util::reference::matchup_statuses(&context.cache, &reference_repo);
    let teams = itry!(team_repo.get_by_league_id(league_id));

    let validation_errors = super::validator::validate(
        &update_team_event_request,
        &statuses,
        &teams.iter().map(|t| t.id).collect(),
        &league.matchup_locations.iter().map(|l| l.id).collect(),
        &existing_team_events.iter().map(|g| g.id()).collect(),
        Some(&existing_team_event));

    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    if update_team_event_request.home_team_id.is_some() { // Matchup 
        let update_dao = build_matchup_dao(&update_team_event_request,
                                           league_id,
                                           team_event_id,
                                           &statuses,
                                           existing_team_event);
        ::util::record::invalidate_records_for_team_id(update_dao.home_team_id, context.cache.clone());
        ::util::record::invalidate_records_for_team_id(update_dao.away_team_id, context.cache.clone());
        itry!(team_event_repo.update_matchup(&update_dao));
    } else { // bye
        let update_dao = build_bye_dao(&update_team_event_request, league_id, team_event_id);
        ::util::record::invalidate_records_for_team_id(update_dao.team_id, context.cache.clone());
        itry!(team_event_repo.update_bye(&update_dao));
    }

    Ok(Response::with(status::Ok))
}

fn build_matchup_dao(req: &UpdateTeamEventRequest,
                     league_id: i32,
                     team_event_id: i32,
                     statuses: &Vec<::util::reference::KeyValue>,
                     existing_team_event: TeamEvent) -> UpdateMatchup {
    let req_games = req.games.as_ref().unwrap();
    let new_games = req_games.iter()
        .filter(|g| g.id.is_none())
        .map(|g| NewGame {
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            sequence: g.sequence,
            notes: g.notes.clone()
        }).collect();
    let update_games = req_games.iter()
        .filter(|g| g.id.is_some())
        .map(|g| Game {
            id: g.id.unwrap(),
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            sequence: g.sequence,
            notes: g.notes.clone()
        }).collect();
    let keep_games: HashSet<i32> = req_games.iter().filter_map(|g| g.id).collect();
    let existing_games: HashSet<i32> = existing_team_event.matchup_ref().unwrap()
        .games.iter().map(|g| g.id).collect();
    let delete_games = existing_games.difference(&keep_games).map(|id| *id).collect();

    UpdateMatchup {
        id: team_event_id,
        datetime: req.datetime.unwrap().naive_utc(),
        matchup_location_id: req.location_id.unwrap(),
        league_id: league_id,
        home_team_id: req.home_team_id.unwrap(),
        away_team_id: req.away_team_id.unwrap(),
        matchup_status_id: statuses.iter().filter(|s| s.value.to_lowercase() == req.status.as_ref().unwrap().to_lowercase()).next().unwrap().key,
        rescheduled_team_event_id: req.rescheduled_team_event_id,
        notes: req.notes.clone(),
        new_games: new_games,
        update_games: update_games,
        delete_games: delete_games
    }
}

fn build_bye_dao(req: &UpdateTeamEventRequest,
                 league_id: i32,
                 team_event_id: i32) -> Bye {
    Bye {
        id: team_event_id,
        datetime: req.bye_date.unwrap().and_time(NaiveTime::from_hms(0, 0, 0)),
        league_id: league_id,
        team_id: req.bye_team_id.unwrap(),
        notes: req.notes.clone()
    }
}

