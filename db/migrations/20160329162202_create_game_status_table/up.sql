CREATE TABLE game_status (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

INSERT INTO game_status (name) values 
    ('HomeTeamWin'),
    ('AwayTeamWin'),
    ('HomeTeamForfeit'),
    ('AwayTeamForfeit'),
    ('Scheduled'),
    ('Cancelled'),
    ('Rescheduled'),
    ('Tie');
