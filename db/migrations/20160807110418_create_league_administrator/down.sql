
DO $$

DECLARE default_contact_email varchar := 'create_league_administrator@migra.tion';
DECLARE default_contact_phone varchar := '1234567890';

BEGIN

ALTER TABLE league
    ADD COLUMN contact_email VARCHAR,
    ADD COLUMN contact_phone VARCHAR;

UPDATE league l SET contact_email = default_contact_email;
UPDATE league l SET contact_phone = default_contact_phone;

ALTER TABLE league
    ALTER COLUMN contact_email SET NOT NULL,
    ALTER COLUMN contact_phone SET NOT NULL;

DROP TABLE league_administrator;

END $$;
