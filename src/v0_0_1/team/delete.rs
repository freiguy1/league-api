use iron::prelude::*;
use iron::status;

use league_db::repo::{TeamRepo, TeamEventRepo, TeamFollowerRepo};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_id = if let Some(team_id) = ::util::get_i32_from_request(&req, "team_id") {
        team_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = TeamRepo::new(&conn);
    let team_event_repo = TeamEventRepo::new(&conn);
    let follower_repo = TeamFollowerRepo::new(&conn);

    let existing_team = match itry!(repo.get_by_id(team_id)) {
        Some(team) => team,
        None => return Ok(Response::with(status::NotFound))
    };

    let team_events = itry!(team_event_repo.get_by_team_ids(&vec![existing_team.id].into_iter().collect()));

    if team_events.len() != 0 {
        let response_body = itry!(::serde_json::to_string(&vec!["Games or byes exist which reference this team".to_string()]));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    itry!(follower_repo.delete_by_team_id(existing_team.id));

    itry!(repo.delete(existing_team.id));

    Ok(Response::with(status::Ok))
}

