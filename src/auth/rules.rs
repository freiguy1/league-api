use context::ApiContext;
use iron::prelude::*;
use iron::{AroundMiddleware, status, Handler};
use diesel::pg::PgConnection;

use league_db::repo::{UserRepo, LeagueRepo, AppAdministratorRepo, TeamRepo, TeamEventRepo};

pub struct UserRequired;

impl AroundMiddleware for UserRequired {
    fn around(self, handler: Box<Handler>) -> Box<Handler> {
        Box::new(UserRequiredHandler { handler: handler }) as Box<Handler>
    }
}

struct UserRequiredHandler<H: Handler> { handler: H }

impl <H: Handler> Handler for UserRequiredHandler<H> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let user_id: Option<i32> = req.extensions.get::<ApiContext>().unwrap().user_id;
        if user_id.is_none() {
            Ok(Response::with((status::Unauthorized)))
        } else {
            self.handler.handle(req)
        }
    }
}

#[derive(Clone, Copy)]
pub struct RuleChecker<R: Rule>(pub R);

impl<R: Rule> AroundMiddleware for RuleChecker<R> {
    fn around(self, handler: Box<Handler>) -> Box<Handler> {
        let checker_handler = RuleCheckerHandler {
            rule: self.0,
            handler: handler
        };
        UserRequired{}.around(Box::new(checker_handler)) as Box<Handler>
    }
}

#[derive(Clone, Copy)]
struct RuleCheckerHandler<H: Handler, R: Rule> {
    rule: R,
    handler: H
}

impl <H: Handler, R: Rule> Handler for RuleCheckerHandler<H, R> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let user_id;
        {
            let context = req.extensions.get::<ApiContext>().unwrap();
            user_id = context.user_id.unwrap();
        }
        if self.rule.check(user_id, req) {
            self.handler.handle(req)
        } else {
            Ok(Response::with((status::Forbidden)))
        }
    }
}

pub trait Rule: Send + Sync + ::std::any::Any + 'static {
    fn check(&self, user_id: i32, req: &Request) -> bool;
}

#[derive(Clone, Copy)]
pub enum LeagueAdminRuleParamType { LeagueId, TeamId, TeamEventId }

#[derive(Clone, Copy)]
pub struct LeagueAdminRule {
    pub param_name: &'static str,
    pub param_type: LeagueAdminRuleParamType
}

impl LeagueAdminRule {
    pub fn check_by_league_id(conn: &PgConnection, league_id: i32, user_id: i32) -> bool {
        let league_repo = LeagueRepo::new(conn);
        let mut administrator_user_ids = match league_repo.get_administrators_by_league_id(league_id) {
            Ok(daos) => daos.into_iter().map(|a| a.user_id),
            _ => return false
        };

        administrator_user_ids.any(|id| id == user_id)
    }

    pub fn check_by_team_id(conn: &PgConnection, team_id: i32, user_id: i32) -> bool {
        let team_repo = TeamRepo::new(conn);
        let league_id = match team_repo.get_by_id(team_id) {
            Ok(Some(dao)) => dao.league_id,
            _ => return false
        };
        Self::check_by_league_id(conn, league_id, user_id)
    }

    pub fn check_by_team_event_id(conn: &PgConnection, team_event_id: i32, user_id: i32) -> bool {
        let team_event_repo = TeamEventRepo::new(conn);
        let league_id = match team_event_repo.get_single(team_event_id) {
            Ok(Some(dao)) => dao.league_id(),
            _ => return false
        };
        Self::check_by_league_id(conn, league_id, user_id)
    }
}

impl Rule for LeagueAdminRule {
    fn check(&self, user_id: i32, req: &Request) -> bool {
        let param_value = match ::util::get_i32_from_request(req, self.param_name) {
            Some(id) => id,
            None => return false
        };

        let context = req.extensions.get::<ApiContext>().unwrap();
        let ref conn = match context.pool.get() {
            Ok(conn) => conn,
            Err(_) => return false
        };

        match self.param_type {
            LeagueAdminRuleParamType::LeagueId => Self::check_by_league_id(&conn, param_value, user_id),
            LeagueAdminRuleParamType::TeamId => Self::check_by_team_id(&conn, param_value, user_id),
            LeagueAdminRuleParamType::TeamEventId => Self::check_by_team_event_id(&conn, param_value, user_id)
        }
    }
}

#[derive(Clone, Copy)]
pub struct ConfirmedUserRule {}

impl Rule for ConfirmedUserRule {
    fn check(&self, user_id: i32, req: &Request) -> bool {
        let context = req.extensions.get::<ApiContext>().unwrap();
        let ref conn = match context.pool.get() {
            Ok(conn) => conn,
            Err(_) => return false
        };

        let user_repo = UserRepo::new(&conn);
        let user_dao = match user_repo.get_single(user_id) {
            Ok(Some(u)) => u,
            _ => return false
        };

        user_dao.is_confirmed
    }
}

#[derive(Clone, Copy)]
pub struct AppAdminRule {}

impl Rule for AppAdminRule {
    fn check(&self, user_id: i32, req: &Request) -> bool {
        let context = req.extensions.get::<ApiContext>().unwrap();
        let ref conn = match context.pool.get() {
            Ok(conn) => conn,
            Err(_) => return false
        };

        let app_admin_repo = AppAdministratorRepo::new(&conn);
        match app_admin_repo.get_by_user_id(user_id) {
            Ok(Some(_)) => true,
            _ => false
        }
    }
}
