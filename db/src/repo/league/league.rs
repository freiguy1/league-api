use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::expression::dsl::*;
use diesel_full_text_search::*;
use schema::league;
use error::Error;
use std::collections::HashSet;

#[derive(Queryable, AsChangeset)]
#[table_name="league"]
#[changeset_options(treat_none_as_null = "true")]
pub struct LeagueDao {
    pub id: i32,
    pub name: String,
    pub information: Option<String>,
    pub sport_id: i32,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: i32
}

#[derive(Insertable)]
#[table_name="league"]
pub struct NewLeague {
    pub name: String,
    pub information: Option<String>,
    pub sport_id: i32,
    pub organization: String,
    pub contact_phone: String,
    pub games_per_matchup: i32
}

pub struct LeagueRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> LeagueRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> LeagueRepo {
        LeagueRepo {
            conn: conn
        }
    }

    pub fn create(&self, new_league: &NewLeague) -> Result<LeagueDao, Error> {
        let result = try!(::diesel::insert(new_league).into(league::table)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn get_single(&self, id: i32) -> Result<Option<LeagueDao>, Error> {
        let result = try!(league::table.filter(league::id.eq(id))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn get_many(&self, ids: &HashSet<i32>) -> Result<Vec<LeagueDao>, Error> {
        let result = try!(league::table.filter(league::id.eq_any(ids)).load(self.conn));
        Ok(result)
    }

    pub fn update(&self, updated_league: &LeagueDao) -> Result<LeagueDao, Error> {
        let result = try!(::diesel::update(league::table.filter(league::id.eq(updated_league.id)))
            .set(updated_league)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete(&self, id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(league::table.filter(league::id.eq(id)))
            .execute(self.conn));
        Ok(result)
    }

    pub fn search(&self, query: &String) -> Result<Vec<LeagueDao>, Error> {
        let query_items: Vec<&str> = query.trim().split_whitespace().collect();
        let query = query_items.join(" & ");
        let tsquery = to_tsquery(query);
        let tsvector = to_tsvector(sql("'english', name || ' ' || organization"));
        let result = try!(league::table.filter(&tsvector.matches(&tsquery)).load::<LeagueDao>(self.conn));
        Ok(result)
    }
}

#[test]
fn search_test() {
    // Flexible to work on CI build or localhost
    let connection_string = ::std::env::var("DATABASE_URL")
        .unwrap_or("postgres://localhost/league_dev?user=league_dev_user&password=league_dev_user_password".to_string());
    let conn = PgConnection::establish(&connection_string).expect("Couldn't unwrap connection");
    let league_repo = LeagueRepo::new(&conn);
    let query = "edit      apples".to_string();
    let leagues = league_repo.search(&query).expect("Error searching leagues");
    println!("leagues.len() = {:?}", leagues.len());
}
