use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use chrono::*;

pub struct Cache {
    data: HashMap<String, CacheItem>,
    expiration_duration: Duration
}

impl Cache {
    pub fn new(duration: Duration) -> Cache {
        Cache {
            data: HashMap::new(),
            expiration_duration: duration
        }
    }

    pub fn get<T: Deserialize>(&self, key: String) -> Option<T> {
        if let Some(item) = self.data.get(&key) {
            if UTC::now() > item.expiration {
                None
            } else {
                let deserialized = item.get_item();
                Some(deserialized)
            }
        } else {
            None
        }
    }

    pub fn insert<T: Serialize>(&mut self, key: String, item: &T) {
        if self.data.contains_key(&key) {
            self.data.remove(&key);
        }

        let cache_item = CacheItem::new(item, UTC::now() + self.expiration_duration);
        self.data.insert(key, cache_item);
    }

    pub fn invalidate(&mut self, key: String) {
        self.data.remove(&key);
    }
}

struct CacheItem {
    pub expiration: DateTime<UTC>,
    serialized: String
}

impl CacheItem {
    pub fn new<T: Serialize>(item: &T, expiration: DateTime<UTC>) -> CacheItem {
        let serialized = ::serde_json::to_string(item).unwrap();
        CacheItem {
            expiration: expiration,
            serialized: serialized
        }
    }

    pub fn get_item<T: Deserialize>(&self) -> T {
        ::serde_json::from_str(&self.serialized).unwrap()
    }
}

