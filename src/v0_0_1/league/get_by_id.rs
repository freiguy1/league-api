use iron::prelude::*;
use iron::status;

use super::contract::{MatchupLocation, GetLeagueResponse, Administrator};
use league_db::repo::{ReferenceRepo, UserRepo, UserDao, LeagueRepo, LeagueDao, TeamRepo};


pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let user_repo = UserRepo::new(&conn);
    let sports = ::util::reference::sports(&context.cache, &reference_repo);
    let team_count = itry!(team_repo.get_count_by_league_ids(&vec![league_id].into_iter().collect()))[&league_id];
    match itry!(repo.get_single(league_id)) {
        Some(dao) => {
            let user_ids : Vec<i32> = dao.administrators.iter().map(|ad| ad.user_id).collect();
            let is_admin = context.user_id.map(|cuid| user_ids.iter().any(|&uid| uid == cuid)).unwrap_or(false);
            let users = itry!(user_repo.get_many(&user_ids));
            let response = build_response(dao, users, &sports, is_admin, team_count);
            let response_body = itry!(::serde_json::to_string(&response));
            Ok(::util::json_response(response_body, status::Ok))
        },
        None => Ok(Response::with(status::NotFound))
    }
}

fn build_response(dao: LeagueDao,
                  admin_users: Vec<UserDao>,
                  sports: &Vec<::util::reference::KeyValue>,
                  is_admin: bool,
                  team_count: i32) -> GetLeagueResponse {
    let sport = sports.iter().filter(|s| s.key == dao.sport_id).next().unwrap().value.clone();

    GetLeagueResponse {
        id: dao.id,
        name: dao.name,
        information: dao.information,
        sport: sport,
        matchup_locations: dao.matchup_locations.into_iter().map(|loc| MatchupLocation {
            id: loc.id,
            name: loc.name
        }).collect(),
        organization: dao.organization,
        contact_phone: dao.contact_phone,
        administrators: if is_admin {
            dao.administrators.into_iter().map(|ad|{
                let user = admin_users.iter().find(|u| u.id == ad.user_id).unwrap();
                Administrator {
                    id: ad.id,
                    user_id: ad.user_id,
                    first_name: user.first_name.clone(),
                    last_name: user.last_name.clone(),
                }
            }).collect()
        } else { Vec::new() },
        team_count: team_count,
        games_per_matchup: dao.games_per_matchup
    }
}
