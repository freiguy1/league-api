use context::ApiContext;
use chrono::*;
use iron::prelude::*;
use iron::AfterMiddleware;
use router::Router;
use jwt::{Header, Token, Registered};
use crypto::sha2::Sha256;
use std::default::Default;
use hyper::header::{Authorization, Bearer};

mod oauth2;
mod contracts;
pub mod rules;

// endpoints
mod confirm_new_user;
mod create_user;
mod login;

pub fn init_routes(router: &mut Router, config: &::context::Config) {
    // Create user
    router.post("user", create_user::handle, "create_user");

    // Login
    router.post("user/login", login::handle, "login_user");

    // Confirm New User
    router.get("user/confirm/:confirmation_key", confirm_new_user::handle, "confirm_new_user");

    oauth2::init_routes(router, config);
}

fn add_token_to_response(response: &mut Response, token: String) {
    response.headers.set(
       Authorization(
           Bearer {
               token: token
           }
       )
    );
}

fn create_token(user_id: i32, config: &::context::Config) -> String {
    let expiration_time = UTC::now() + Duration::minutes(config.token_expiration_minutes as i64);
    let exp_micros = expiration_time.timestamp() as u64;
    let header: Header = Default::default();
        let claims = Registered {
            iss: Some(config.app_name.clone()),
            sub: Some(user_id.to_string()),
            exp: Some(exp_micros),
            ..Default::default()
        };
        let token = Token::new(header, claims);

        token.signed(config.secret.as_ref(), Sha256::new()).unwrap()
}


pub struct TokenRefresh;

impl AfterMiddleware for TokenRefresh {
    fn after(&self, req: &mut Request, mut res: Response) -> IronResult<Response> {
        let context = req.extensions.get::<ApiContext>().unwrap();
        match context.user_id {
            Some(uid) => {
                let ref config = context.config;
                let token = create_token(uid, &config);
                add_token_to_response(&mut res, token);
                Ok(res)
            },
            None => Ok(res)
        }
    }
}


