use iron::prelude::*;
use iron::status;

use super::contract::{GetTeamEventResponse, Game};
use league_db::repo::{TeamEventRepo, TeamEvent, Matchup, Bye, ReferenceRepo};
use chrono::{UTC, DateTime};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_event_id = if let Some(team_event_id) = ::util::get_i32_from_request(&req, "team_event_id") {
        team_event_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_event_id in url should be a number")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let statuses = ::util::reference::matchup_statuses(&context.cache, &reference_repo);

    let dao = match itry!(team_event_repo.get_single(team_event_id)) {
        Some(te) => te,
        None => return Ok(Response::with(status::NotFound))
    };

    let team_event = match dao {
        TeamEvent::Matchup(m) => build_response_from_matchup(m, &statuses),
        TeamEvent::Bye(b) => build_response_from_bye(b),
    };

    let response_body = itry!(::serde_json::to_string(&team_event));
    Ok(::util::json_response(response_body, status::Ok))
}

fn build_response_from_matchup(m: Matchup, statuses: &Vec<::util::reference::KeyValue>) -> GetTeamEventResponse {
    let games = m.games.iter().map(|g| Game {
        id: g.id,
        sequence: g.sequence,
        home_team_score: g.home_team_score,
        away_team_score: g.away_team_score,
        notes: g.notes.clone()
    }).collect();

    GetTeamEventResponse {
        id: m.id,
        notes: m.notes.clone(),
        datetime: Some(DateTime::from_utc(m.datetime, UTC)),
        location_id: Some(m.matchup_location_id),
        status: Some(statuses.iter().filter(|s| s.key == m.matchup_status_id).next().unwrap().value.clone()),
        rescheduled_team_event_id: m.rescheduled_team_event_id,
        home_team_id: Some(m.home_team_id),
        away_team_id: Some(m.away_team_id),
        bye_date: None,
        bye_team_id: None,
        games: Some(games)
    }

}

fn build_response_from_bye(b: Bye) -> GetTeamEventResponse {
    GetTeamEventResponse {
        id: b.id,
        notes: b.notes.clone(),
        datetime: None,
        location_id: None,
        status: None,
        rescheduled_team_event_id: None,
        home_team_id: None,
        away_team_id: None,
        bye_date: Some(b.datetime.date()),
        bye_team_id: Some(b.team_id),
        games: None
    }
}
