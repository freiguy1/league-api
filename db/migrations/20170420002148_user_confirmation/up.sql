ALTER TABLE league_user ADD CONSTRAINT email_unique UNIQUE (email);

ALTER TABLE league_user ADD COLUMN is_confirmed BOOLEAN;
UPDATE league_user SET is_confirmed = true;
ALTER TABLE league_user ALTER COLUMN is_confirmed SET NOT NULL;

ALTER TABLE league_user ADD COLUMN confirmation_key VARCHAR(36);
UPDATE league_user SET confirmation_key = league_user.email;
ALTER TABLE league_user ALTER COLUMN confirmation_key SET NOT NULL;
ALTER TABLE league_user ADD CONSTRAINT confirmation_key_unique UNIQUE (confirmation_key);

ALTER TABLE league_user ADD COLUMN created_datetime TIMESTAMP;
UPDATE league_user set created_datetime = now();
ALTER TABLE league_user ALTER COLUMN created_datetime SET NOT NULL;

