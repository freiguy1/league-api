CREATE TABLE team (
    id SERIAL PRIMARY KEY,
    number INTEGER NOT NULL,
    league_id INTEGER NOT NULL REFERENCES league(id),
    name VARCHAR NOT NULL,
    captain_name VARCHAR NOT NULL,
    captain_email VARCHAR NOT NULL,
    captain_phone VARCHAR NOT NULL
);
