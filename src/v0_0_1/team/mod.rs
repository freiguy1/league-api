use iron::AroundMiddleware;
use router::Router;

use ::auth::rules::{RuleChecker, LeagueAdminRule, LeagueAdminRuleParamType};

pub mod contract;

mod create;
mod update;
mod get_all;
mod get_by_id;
mod delete;
mod validator;

// TEAM MODULE
pub fn init_routes(router: &mut Router) {
    let league_admin_rule = RuleChecker(LeagueAdminRule {
        param_name: "league_id" ,
        param_type: LeagueAdminRuleParamType::LeagueId
    });

    let league_admin_teamid_rule = RuleChecker(LeagueAdminRule {
        param_name: "team_id",
        param_type: LeagueAdminRuleParamType::TeamId
    });


    // Get single
    let handler = Box::new(get_by_id::handle);
    let handler = ::arounds::HttpCacheHeader{}.around(handler);
    router.get("teams/:team_id", handler, "get_team");
    // Get all
    let handler = Box::new(get_all::handle);
    let handler = ::arounds::HttpCacheHeader{}.around(handler);
    router.get("leagues/:league_id/teams", handler, "get_all_teams");


    // Delete
    router.delete("teams/:team_id", league_admin_teamid_rule.around(Box::new(delete::handle)), "delete_team");
    // Create
    router.post("leagues/:league_id/teams", league_admin_rule.around(Box::new(create::handle)), "create_team");
    // Update
    router.put("teams/:team_id", league_admin_teamid_rule.around(Box::new(update::handle)), "update_team");
}
