use iron::prelude::*;
use router::Router;
use iron::status;
use serde::de::Deserialize;
use std::any::Any;
use std::clone::Clone;
use iron::headers::{ ContentType, Headers };

mod logger;
mod cache;
mod emailer;
pub mod reference;
pub mod record;
pub mod validate;

pub use self::logger::*;
pub use self::cache::*;
pub use self::emailer::*;


pub fn json_response(json_body: String, status: status::Status) -> Response {
    let mut headers = Headers::new();
    headers.set(ContentType::json());
    let mut response = Response::with((status, json_body));
    response.headers = headers;
    response
}

pub fn get_string_from_request(req: &Request, param_name: &'static str) -> Option<String> {
    req.extensions.get::<Router>().unwrap().find(param_name).map(|s| s.to_string())
}

pub fn get_i32_from_request(req: &Request, param_name: &'static str) -> Option<i32> {
    let param  = req.extensions.get::<Router>().unwrap().find(param_name);
    let param = if let Some(param) = param {
        param
    } else {
        // TODO should probably be error.
        return None;
    };

    // Convert from string to i32
    let param: i32 = if let Ok(param) = param.parse() {
        param
    } else {
        return None;
    };
    Some(param)
}

pub fn parse_body<T: Deserialize + Any + Clone>(req: &mut Request) -> Result<T, Response> {
    let parsed = req.get::<::bodyparser::Struct<T>>();
    match parsed {
        Ok(Some(success)) => Ok(success),
        Ok(None) => Err(Response::with((status::BadRequest, "Endpoint requires json body"))),
        Err(err) => Err(Response::with((status::BadRequest, format!("{:?}", err))))
    }
}
