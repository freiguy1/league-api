-- Your SQL goes here
CREATE INDEX league_search_idx
ON league
USING GIN (to_tsvector('english', name || ' ' || organization));
