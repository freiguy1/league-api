use iron::prelude::*;
use iron::status;

use league_db::repo::{LeagueRepo, TeamEventRepo, TeamEvent};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_event_id = if let Some(team_event_id) = ::util::get_i32_from_request(&req, "team_event_id") {
        team_event_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_event_id in url should be a u32")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);

    let dao_to_delete = match itry!(team_event_repo.get_single(team_event_id)) {
        Some(te) => te,
        None => return Ok(Response::with(status::NotFound))
    };
    let league_id = dao_to_delete.league_id();

    let existing_team_events = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));

    match dao_to_delete {
        TeamEvent::Matchup(ref m) => {
            ::util::record::invalidate_records_for_team_id(m.home_team_id, context.cache.clone());
            ::util::record::invalidate_records_for_team_id(m.away_team_id, context.cache.clone());
        },
        TeamEvent::Bye(ref b) =>
            ::util::record::invalidate_records_for_team_id(b.team_id, context.cache.clone())
    }

    if any_team_event_has_rteid(&existing_team_events, team_event_id) {
        return Ok(Response::with((status::BadRequest, "This team_event was rescheduled and is linked to another team_event. Delete linked team_event first.")));
    }

    itry!(team_event_repo.delete_many(&vec![team_event_id]));

    Ok(Response::with(status::Ok))
}

fn any_team_event_has_rteid(team_events: &Vec<TeamEvent>, team_event_id: i32) -> bool {
    let mut result = false;
    for te in team_events.iter() {
        result = result | match te {
            &TeamEvent::Matchup(ref m) => m.rescheduled_team_event_id.map(|r| r == team_event_id).unwrap_or(false),
            _ => false
        };
    }
    result
}
