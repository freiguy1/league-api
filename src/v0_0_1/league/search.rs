use iron::prelude::*;
use iron::status;
use hyper::header::AccessControlAllowOrigin;
use std::collections::HashMap;
use urlencoded::UrlEncodedQuery;

use super::contract::SearchLeaguesResponseItem;
use league_db::repo::{ReferenceRepo, LeagueRepo, LeagueDao, TeamRepo};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let query_string_map = match req.get::<UrlEncodedQuery>() {
        Ok(hashmap) => hashmap,
        Err(_) => return Ok(Response::with(status::BadRequest))
    };

    let query = Query {
        q: query_string_map
            .get("q")
            .and_then(|v| v.first())
            .map(|v| v.to_string()),
    };

    // If query contains nothing, improper search
    if query.is_empty_query() { return Ok(Response::with(status::BadRequest)); }
    if !query.is_valid() { return Ok(Response::with(status::BadRequest)); }

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let sports = ::util::reference::sports(&context.cache, &reference_repo);
    let daos = itry!(repo.search(query.q.as_ref().unwrap()));
    let league_ids = daos.iter().map(|d| d.id).collect();
    let team_counts = itry!(team_repo.get_count_by_league_ids(&league_ids));
    let leagues = daos.into_iter().map(|dao|build_response(dao, &sports, &team_counts)).collect::<Vec<_>>();
    let response_body = itry!(::serde_json::to_string(&leagues));
    let mut response = ::util::json_response(response_body, status::Ok);
    // resp.headers.set(CacheControl(vec![CacheDirective::MaxAge(600u32)]));
    response.headers.set(AccessControlAllowOrigin::Any);
    Ok(response)
}

struct Query {
    q: Option<String>
}

impl Query {
    fn is_empty_query(&self) -> bool {
        self.q.is_none() 
    }

    fn is_valid(&self) -> bool {
        self.q_is_valid()
    }

    fn q_is_valid(&self) -> bool {
        self.q.is_none() || self.q.as_ref().unwrap().len() > 2
    }
}

fn build_response(dao: LeagueDao,
                  sports: &Vec<::util::reference::KeyValue>,
                  team_counts: &HashMap<i32, i32>) -> SearchLeaguesResponseItem {
    let sport = sports.iter().filter(|s| s.key == dao.sport_id).next().unwrap().value.clone();

    SearchLeaguesResponseItem {
        id: dao.id,
        name: dao.name,
        information: dao.information,
        sport: sport,
        organization: dao.organization,
        team_count: team_counts[&dao.id]
    }
}
