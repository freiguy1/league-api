use chrono::*;
use iron::prelude::*;
use iron::{BeforeMiddleware, typemap};
use std::sync::{Mutex, Arc};
use diesel::pg::PgConnection;
use r2d2_diesel::ConnectionManager;
use jwt::{Header, Token, Registered};
use crypto::sha2::Sha256;
use hyper::header::{Authorization, Bearer};
use std::ops::Deref;
use util::Emailer;
use url::Url;

const DEFAULT_CONNECTION_STRING: &'static str = "postgres://localhost/league_dev?user=league_dev_user&password=league_dev_user_password";
const DEFAULT_CACHE_EXPIRATION_MINUTES: i64 = 1440; // 1 day
const DEFAULT_PORT: u16 = 6767;
const DEFAULT_BASE_URL: &'static str = "http://localhost:6767";
const DEFAULT_SECRET: &'static str = "fMrA3ILGLgJqkaEGgECuhQ34TziZEX";
const DEFAULT_TOKEN_EXPIRATION_MINUTES: u16 = 24*60; // 60 minutes * 24 = 24 hours

pub struct ApiContext {
    pub pool: ::r2d2::Pool<ConnectionManager<PgConnection>>,
    pub cache: Arc<Mutex<::util::Cache>>,
    pub user_id: Option<i32>,
    pub config: Config
}

impl typemap::Key for ApiContext { type Value = ApiContext; }

impl ApiContext {
    pub fn emailer(&self) -> Option<Emailer> {
        self.config.email.as_ref().map(|email|
            Emailer::new(&email.from_email,
                         &email.from_name,
                         &email.username,
                         &email.password,
                         &email.server,
                         email.port,
                         self.pool.clone()))
    }

    pub fn logger(&self) -> ::util::Logger {
        ::util::Logger::new(self.pool.clone())
    }
}

pub struct ContextManager {
    pool: ::r2d2::Pool<ConnectionManager<PgConnection>>,
    cache: Arc<Mutex<::util::Cache>>,
    pub config: Config
}

impl ContextManager {
    pub fn new() -> ContextManager {
        let config = ContextManager::load_config();
        let r2d2_config = ::r2d2::Config::default();
        let manager = ConnectionManager::<PgConnection>::new(config.connection_string.as_ref());
        let pool = ::r2d2::Pool::new(r2d2_config, manager).expect("Failed to create db pool");
        let cache = ::util::Cache::new(::chrono::Duration::minutes(config.cache_timeout_minutes));

        ContextManager {
            pool: pool,
            cache: Arc::new(Mutex::new(cache)),
            config: config
        }
    }

    fn get_test_user_id(&self, email: &String) -> Option<i32> {
        let ref conn = match self.pool.get() {
            Ok(c) => c,
            _ => return None
        };
        let user_repo = ::league_db::repo::UserRepo::new(&conn);
        if let Ok(test_user_dao) = user_repo.get_by_email(email) {
            test_user_dao.map(|dao| dao.id)
        } else { None }
    }

    fn user_id_from_request(&self, req: &Request) -> Option<i32> {
        let ref secret = self.config.secret;
        let header = match req.headers.get::<Authorization<Bearer>>() {
            Some(header) => header,
            None => return None
        };

        // Check for test user if one's available
        if let Some(ref test_user) = self.config.test_user {
            if header.deref().token == test_user.bearer_token {
                if let Some(id) = self.get_test_user_id(&test_user.email) {
                    return Some(id);
                }
            }
        }

        let token = match Token::<Header, Registered>::parse(&header.deref().token) {
            Ok(token) => token,
            Err(_) => return None
        };


        if !token.verify(secret.as_ref(), Sha256::new()) {
            return None;
        }

        if token.claims.exp
            .map(|exp| NaiveDateTime::from_timestamp(exp as i64, 0))
            .map(|exp| DateTime::from_utc(exp, UTC))
            .map(|exp| UTC::now() > exp)
            .unwrap_or(true) {
            return None;
        }

        token.claims.sub.and_then(|sub| sub.parse().ok())
    }

    fn load_config() -> Config {
        use std::env;
        let connection_string = if let Ok(var) = env::var("DATABASE_URL") { var }
        else { DEFAULT_CONNECTION_STRING.to_string() };

        let port: u16 = if let Ok(var) = env::var("PORT") {
            var.parse().expect("Unable to parse port from env var")
        } else { DEFAULT_PORT };

        let cache_expiration_minutes = if let Ok(var) = env::var("CACHE_EXPIRATION_MINUTES") {
            var.parse().expect("Unable to parse cache expiration minutes from env var")
        } else { DEFAULT_CACHE_EXPIRATION_MINUTES };

        let secret = if let Ok(var) = env::var("SECRET") { var }
        else { DEFAULT_SECRET.to_string() };

        let token_expiration_minutes = if let Ok(var) = env::var("TOKEN_EXPIRATION_MINUTES") {
            var.parse().expect("Unable to parse token expiration minutes from env var")
        } else { DEFAULT_TOKEN_EXPIRATION_MINUTES };

        let base_url = if let Ok(var) = env::var("BASE_URL") {
            Url::parse(&var).expect("Unable to parse base url from env var")
        } else { Url::parse(DEFAULT_BASE_URL).unwrap() };

        let test_user_bearer_token = env::var("TEST_USER_BEARER_TOKEN");
        let test_user_email = env::var("TEST_USER_EMAIL");
        let test_user = if let (Ok(token), Ok(email)) = (test_user_bearer_token, test_user_email) {
            Some(TestUser{
                bearer_token: token,
                email: email
            })
        } else { None };

        let email_username = env::var("EMAIL_USERNAME");
        let email_password = env::var("EMAIL_PASSWORD");
        let email_server = env::var("EMAIL_SERVER");
        let email_port = env::var("EMAIL_PORT");
        let email_from_email = env::var("EMAIL_FROM_EMAIL");
        let email_from_name = env::var("EMAIL_FROM_NAME");

        let email = if email_username.is_ok() &&
            email_password.is_ok() &&
            email_server.is_ok() &&
            email_port.is_ok() &&
            email_from_email.is_ok() &&
            email_from_name.is_ok() {
            Some(EmailConfig {
                username: email_username.unwrap(),
                password: email_password.unwrap(),
                server: email_server.unwrap(),
                port: email_port.unwrap().parse().expect("couldn't parse port from env var"),
                from_email: email_from_email.unwrap(),
                from_name: email_from_name.unwrap()
            })
        } else { None };

        let google_client_id = env::var("GOOGLE_CLIENT_ID");
        let google_client_secret = env::var("GOOGLE_CLIENT_SECRET");
        let google_auth = if let (Ok(id), Ok(secret)) = (google_client_id, google_client_secret) {
            Some(GoogleAuth {
                client_id: id,
                client_secret: secret
            })
        } else { None };

        Config {
            cache_timeout_minutes: cache_expiration_minutes,
            connection_string: connection_string,
            port: port,
            secret: secret,
            app_name: "League API".to_string(),
            token_expiration_minutes: token_expiration_minutes,
            base_url: base_url,
            test_user: test_user,
            email: email,
            google_auth: google_auth,
        }

    }
}

impl BeforeMiddleware for ContextManager {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        let user_id_opt = self.user_id_from_request(&req);

        let api_context = ApiContext {
            pool: self.pool.clone(),
            cache: self.cache.clone(),
            user_id: user_id_opt,
            config: self.config.clone()
        };
        req.extensions.insert::<ApiContext>(api_context);
        Ok(())
    }
}


#[derive(Deserialize, Debug, Clone)]
pub struct GoogleAuth {
    pub client_id: String,
    pub client_secret: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct TestUser {
    email: String,
    bearer_token: String
}

#[derive(Deserialize, Debug, Clone)]
pub struct EmailConfig {
    username: String,
    password: String,
    server: String,
    port: u16,
    from_email: String,
    from_name: String
}

#[derive(Clone)]
pub struct Config {
    pub cache_timeout_minutes: i64,
    pub connection_string: String,
    pub port: u16,
    pub secret: String,
    pub app_name: String,
    pub token_expiration_minutes: u16,
    pub test_user: Option<TestUser>,
    pub email: Option<EmailConfig>,
    pub google_auth: Option<GoogleAuth>,
    pub base_url: Url
}
