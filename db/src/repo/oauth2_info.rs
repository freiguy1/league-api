use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::oauth2_info;
use error::Error;

#[derive(Queryable, AsChangeset)]
#[table_name="oauth2_info"]
#[changeset_options(treat_none_as_null = "true")]
pub struct OAuth2InfoDao {
    pub id: i32,
    pub provider: String,
    pub profile_id: String,
    pub email: String,
    pub user_id: Option<i32>
}

#[derive(Insertable)]
#[table_name="oauth2_info"]
struct NewOAuth2Info {
    pub provider: String,
    pub profile_id: String,
    pub email: String,
    pub user_id: Option<i32>
}

pub struct OAuth2InfoRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> OAuth2InfoRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> OAuth2InfoRepo {
        OAuth2InfoRepo {
            conn: conn
        }
    }

    pub fn create(&self, provider: &str, profile_id: &str, email: &str, user_id: Option<i32>) -> Result<OAuth2InfoDao, Error> {
        let new_row = NewOAuth2Info {
            provider: provider.to_string(),
            profile_id: profile_id.to_string(),
            email: email.to_string(),
            user_id: user_id
        };
        let result = try!(::diesel::insert(&new_row).into(oauth2_info::table)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn get_by_id(&self, id: i32) -> Result<Option<OAuth2InfoDao>, Error> {
        let result = try!(oauth2_info::table.filter(oauth2_info::id.eq(id))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn get_by_provider_profile_id(&self, provider: &str, profile_id: &str) -> Result<Option<OAuth2InfoDao>, Error> {
        let result = try!(oauth2_info::table.filter(oauth2_info::provider.eq(provider).and(oauth2_info::profile_id.eq(profile_id)))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn update(&self, id: i32, provider: &str, profile_id: &str, email: &str, user_id: Option<i32>) -> Result<OAuth2InfoDao, Error> {
        let updated_row = OAuth2InfoDao {
            id: id,
            provider: provider.to_string(),
            profile_id: profile_id.to_string(),
            email: email.to_string(),
            user_id: user_id
        };
        let result = try!(::diesel::update(oauth2_info::table.filter(oauth2_info::id.eq(id)))
            .set(&updated_row)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn update_user_id(&self, id: i32, user_id: Option<i32>) -> Result<(), Error> {
        try!(::diesel::update(oauth2_info::table.filter(oauth2_info::id.eq(id)))
            .set(oauth2_info::user_id.eq(user_id))
            .execute(self.conn));
        Ok(())
    }
}
