use std::collections::HashSet;
use league_db::repo::{LeagueDao, UserDao};

pub fn validate<L: League>(
    l: &mut L,
    sports: &Vec<::util::reference::KeyValue>,
    used_location_ids: &HashSet<i32>,
    current_league: Option<&LeagueDao>,
    request_users: &Vec<UserDao>) -> Vec<String> {
    let mut result = Vec::new();
    let request_location_ids: HashSet<i32> = l.get_matchup_locations().iter().filter_map(|loc| loc.get_id()).collect();
    let current_location_ids: HashSet<i32> = 
        current_league
            .map(|l| l.matchup_locations.iter().map(|loc| loc.id).collect())
            .unwrap_or(HashSet::new());
    let request_admin_ids: HashSet<i32> = l.get_administrators().iter().filter_map(|ad| ad.get_id()).collect();
    let current_admin_ids: HashSet<i32> =
        current_league
        .map(|l| l.administrators.iter().map(|ad| ad.id).collect())
        .unwrap_or(HashSet::new());

    if l.get_name().len() > 45 || l.get_name().len() < 5 {
        result.push("name must be between 5 and 45 characters".to_string());
    }
    let sport_opt = sports
        .into_iter()
        .filter(|s| s.value.to_lowercase() == l.get_sport().to_lowercase())
        .next();
    if sport_opt.is_none() {
        result.push("sport must contain a valid sport choice".to_string());
    }

    if request_location_ids.len() != l.get_matchup_locations().iter().filter(|loc| loc.get_id().is_some()).count() {
        result.push("location id is used more than once".to_string());
    }
    if !request_location_ids.iter().all(|loc_id| current_location_ids.contains(&loc_id)) {
        result.push("location ids must be valid for given league".to_string());
    }
    if !used_location_ids.is_subset(&request_location_ids) {
        result.push("cannot delete locations which are used by matchups".to_string());
    }
    if l.get_organization().len() > 45 || l.get_organization().len() < 3 {
        result.push("organization must be between 3 and 45 characters".to_string());
    }
    if let Err(msg) = ::util::validate::is_valid_phone(&mut l.get_contact_phone()) {
        result.push(msg.to_string());
    }
    if current_league.is_some() && l.get_administrators().len() == 0 {
        result.push("there must remain 1 administrator".to_string());
    }
    if request_users.len() != l.get_administrators().len() {
        result.push("one or more user_ids invalid".to_string());
    }
    if request_admin_ids.len() != l.get_administrators().iter().filter(|ad| ad.get_id().is_some()).count() {
        result.push("administrator id used more than once".to_string());
    }
    if !request_admin_ids.iter().all(|a_id| current_admin_ids.contains(&a_id)) {
        result.push("administrator ids must be valid for given league".to_string());
    }

    result
}

pub trait League {
    fn get_name(&self) -> &String;
    fn get_information(&self) -> Option<&String>;
    fn get_sport(&self) -> &String;
    fn get_organization(&self) -> &String;
    fn get_contact_phone(&mut self) -> &mut String;
    fn get_games_per_matchup(&self) -> u8;
    fn get_matchup_locations(&self) -> Vec<&MatchupLocation>;
    fn get_administrators(&self) -> Vec<&LeagueAdministrator>;
}

pub trait MatchupLocation {
    fn get_id(&self) -> Option<i32>;
    fn get_name(&self) -> &String;
}

pub trait LeagueAdministrator {
    fn get_id(&self) -> Option<i32>;
    fn get_user_id(&self) -> i32;
}

impl League for super::contract::CreateLeagueRequest {
    fn get_name(&self) -> &String { &self.name }
    fn get_information(&self) -> Option<&String> { self.information.as_ref() }
    fn get_sport(&self) -> &String { &self.sport }
    fn get_organization(&self) -> &String { &self.organization }
    fn get_contact_phone(&mut self) -> &mut String { &mut self.contact_phone }
    fn get_games_per_matchup(&self) -> u8 { self.games_per_matchup }
    fn get_matchup_locations(&self) -> Vec<&MatchupLocation> {
        self.matchup_locations.iter().map(|ml| ml as &MatchupLocation).collect()
    }
    fn get_administrators(&self) -> Vec<&LeagueAdministrator> { vec![] }
}

impl MatchupLocation for String {
    fn get_id(&self) -> Option<i32> { None }
    fn get_name(&self) -> &String { &self }
}

impl League for super::contract::UpdateLeagueRequest {
    fn get_name(&self) -> &String { &self.name }
    fn get_information(&self) -> Option<&String> { self.information.as_ref() }
    fn get_sport(&self) -> &String { &self.sport }
    fn get_organization(&self) -> &String { &self.organization }
    fn get_contact_phone(&mut self) -> &mut String { &mut self.contact_phone }
    fn get_games_per_matchup(&self) -> u8 { self.games_per_matchup }
    fn get_matchup_locations(&self) -> Vec<&MatchupLocation> {
        self.matchup_locations.iter().map(|ml| ml as &MatchupLocation).collect()
    }
    fn get_administrators(&self) -> Vec<&LeagueAdministrator> { 
        self.administrators.iter().map(|ml| ml as &LeagueAdministrator).collect()
    }
}

impl MatchupLocation for super::contract::UpdateMatchupLocation {
    fn get_id(&self) -> Option<i32> { self.id }
    fn get_name(&self) -> &String { &self.name }
}

impl LeagueAdministrator for super::contract::UpdateLeagueAdministrator {
    fn get_id(&self) -> Option<i32> { self.id }
    fn get_user_id(&self) -> i32 { self.user_id }
}

