use iron::AroundMiddleware;
use router::Router;
use ::auth::rules::{RuleChecker, ConfirmedUserRule};

pub mod contract;

mod get_teams;
mod follow_team;
mod unfollow_team;

// MY MODULE
pub fn init_routes(router: &mut Router) {
    let confirmed_user_rule = RuleChecker(ConfirmedUserRule{});
    // Get My Teams
    let handler = Box::new(get_teams::handle);
    let handler = ::arounds::HttpCacheHeader{}.around(handler);
    let handler = confirmed_user_rule.around(handler);
    router.get("my/teams", handler, "get_my_teams");
    // Follow team
    router.post("my/teams/:team_id",
                confirmed_user_rule.around(Box::new(follow_team::handle)),
                "follow_team");
    // Unfollow team
    router.delete("my/teams/:team_id",
                confirmed_user_rule.around(Box::new(unfollow_team::handle)),
                "unfollow_team");
}
