use regex::Regex;

const PHONE_REGEX: &'static str = r"^[0-9]{10}$";
const EMAIL_REGEX: &'static str = r"^\w+[\w-\.]*@\w+((-\w+)|(\w*))\.[a-z]{2,3}$";

pub fn is_valid_phone(phone: &mut String) -> Result<(), &'static str> {
    *phone = filter_phone(&phone);
    let re = Regex::new(PHONE_REGEX).unwrap();
    if !re.is_match(&phone) {
        Err("phones must contain only digits, parenthesis, spaces, and dashes, and there must be 10 digits")
    } else {
        Ok(())
    }
}

pub fn is_valid_email(email: &mut String) -> Result<(), &'static str> {
    *email = email.to_lowercase();
    let re = Regex::new(EMAIL_REGEX).unwrap();
    if !re.is_match(email) {
        Err("emails must be valid emails")
    } else {
        Ok(())
    }
}

fn filter_phone(phone: &String) -> String {
    phone
        .chars()
        .filter(|&c| c != '(')
        .filter(|&c| c != ')')
        .filter(|&c| c != '-')
        .filter(|&c| c != ' ')
        .collect::<String>()
}
