use error::Error;
use lettre::email::EmailBuilder;
use diesel::pg::PgConnection;
use lettre::transport::smtp::{SecurityLevel, SmtpTransportBuilder};
use lettre::transport::EmailTransport;
use r2d2::Pool;
use r2d2_diesel::ConnectionManager;
use std::thread;
use std::error::Error as StdError;

#[derive(Clone)]
pub struct Emailer {
    from_email: String,
    from_name: String,
    username: String,
    password: String,
    server: String,
    port: u16,
    pool: Pool<ConnectionManager<PgConnection>>
}

impl Emailer {
    pub fn new(from_email: &str, from_name: &str, username: &str, password: &str, server: &str, port: u16, pool: Pool<ConnectionManager<PgConnection>>) -> Emailer {
        Emailer {
            from_email: from_email.to_string(),
            from_name: from_name.to_string(),
            username: username.to_string(),
            password: password.to_string(),
            server: server.to_string(),
            port: port,
            pool: pool
        }
    }

    fn send(&self, to_email: &str, to_name: &str, subject: &str, text: &str) -> Result<(), Error> {
        let from_email: &str = &self.from_email;
        let from_name: &str = &self.from_name;
        let email = try!(EmailBuilder::new()
            .to((to_email, to_name))
            .from((from_email, from_name))
            .subject(subject)
            .text(text)
            .build());

        let server: &str = &self.server;
        let mut mailer = try!(SmtpTransportBuilder::new((server, self.port)))
            .credentials(&self.username, &self.password)
            .security_level(SecurityLevel::AlwaysEncrypt)
            .build();

        // Send the email
        try!(mailer.send(email));
        mailer.close();
        Ok(())
    }

    pub fn send_async(&self,
                      to_email: &str,
                      to_name: &str,
                      subject: &str,
                      text: &str) -> thread::JoinHandle<Result<(), Error>> {
        let to_email = to_email.to_string();
        let to_name = to_name.to_string();
        let subject = subject.to_string();
        let text = text.to_string();
        let cloned_self = self.clone();

        thread::spawn(move || {
            match cloned_self.send(&to_email, &to_name, &subject, &text) {
                Err(e) => {
                    let logger = ::util::Logger::new(cloned_self.pool.clone());
                    logger.log_custom(
                        "Error while emailing",
                        &e.description(),
                        ::util::Severity::Error,
                        None,
                        None,
                        None);
                    Err(e)
                },
                _ => Ok(())
            }
        })
    }
}

/*
#[test]
fn email_test() {
    let emailer = Emailer::new(
        "info@justplay.io",
        "JustPlay Information",
        "info@justplay.io",
        "secret",
        "mail.privateemail.com",
        587);

    let result = emailer.send(
        "ethan.frei@gmail.com",
        "Ethan Frei",
        "Test Email",
        "Body of the test email!!");
    println!("emailing result: {:?}", result);
    assert!(result.is_ok());
}

#[test]
fn email_async_test() {
    let emailer = Emailer::new(
        "info@justplay.io",
        "JustPlay Information",
        "info@justplay.io",
        "secret",
        "mail.privateemail.com",
        587);

    let handle = emailer.send_async(
        "ethan.frei@gmail.com",
        "Ethan Frei",
        "Test Asynchronous Email",
        "Body of the test asynchronous email!!");
    let result = handle.join().expect("Couldn't join thread handle");
    println!("emailing result: {:?}", result);
    assert!(result.is_ok());
}

*/
