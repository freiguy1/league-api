use iron::prelude::*;
use iron::status;

use super::contract::UpdateTeamRequest;
use league_db::repo::{TeamRepo, TeamDao};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let team_id = if let Some(team_id) = ::util::get_i32_from_request(&req, "team_id") {
        team_id
    } else {
        return Ok(Response::with((status::BadRequest, "team_id in url should be a number")));
    };

    let mut update_team_request = match ::util::parse_body::<UpdateTeamRequest>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let validation_errors = super::validator::validate(&mut update_team_request);
    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = TeamRepo::new(&conn);

    let existing_team = match itry!(repo.get_by_id(team_id)) {
        Some(team) => team,
        None => return Ok(Response::with(status::NotFound))
    };

    let dao = build_dao(existing_team, update_team_request);

    itry!(repo.update(&dao));

    Ok(Response::with(status::Ok))
}

fn build_dao(existing_team: TeamDao, req: UpdateTeamRequest) -> TeamDao {
    TeamDao {
        id: existing_team.id,
        sequence: existing_team.sequence,
        league_id: existing_team.league_id,
        name: req.name,
        captain_name: req.captain_name,
        captain_email: req.captain_email,
        captain_phone: req.captain_phone
    }
}
