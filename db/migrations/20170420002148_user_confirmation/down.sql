ALTER TABLE league_user DROP COLUMN IF EXISTS is_confirmed;
ALTER TABLE league_user DROP COLUMN IF EXISTS confirmation_key;
ALTER TABLE league_user DROP COLUMN IF EXISTS created_datetime;
