#[derive(Debug, Clone, Serialize)]
pub struct CreateUserFromOAuth2Response {
    pub email: String,
    pub first_name: String,
    pub last_name: String,
    pub phone: Option<String>,
    pub oauth2_info_id: i32
}

#[derive(Debug, Clone, Serialize)]
pub struct SuccessfulOAuth2Response {
    pub token: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct AuthorizeUrlResponseItem {
    pub provider: &'static str,
    pub url: String
}

#[derive(Debug, Clone, Deserialize)]
pub struct CreateUserRequest {
    pub email: String,
    pub password: Option<String>,
    pub first_name: String,
    pub last_name: String,
    pub phone: String,
    pub oauth2_info_id: Option<i32>
}

impl CreateUserRequest {
    pub fn validate(&mut self) -> Vec<String> {
        let mut result = Vec::new();
        if self.first_name.len() > 45 || self.first_name.len() < 1 {
            result.push("name must be between 1 and 45 characters".to_string());
        }

        if self.last_name.len() > 45 || self.last_name.len() < 1 {
            result.push("last name must be between 1 and 45 characters".to_string());
        }

        if let Err(msg) = ::util::validate::is_valid_email(&mut self.email) {
            result.push(msg.to_string());
        }

        if let Err(msg) = ::util::validate::is_valid_phone(&mut self.phone) {
            result.push(msg.to_string());
        }

        if let Some(ref password) = self.password {
            if self.oauth2_info_id.is_some() {
                result.push("cannot have both oauth2_info_id and password".to_string());
            }
            if password.len() > 20 || password.len() < 6 {
                result.push("password must be between 6 and 20 characters".to_string());
            }
        }

        if self.password.is_none() && self.oauth2_info_id.is_none() {
            result.push("need either an oauth2_info_id or password".to_string());
        }

        result
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct CreateUserResponse {
    pub token: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct LoginRequest {
    pub email: String,
    pub password: String
}

#[derive(Debug, Clone, Serialize)]
pub struct LoginResponse {
    pub token: String,
}
