use iron::AroundMiddleware;
use ::auth::rules::{RuleChecker, LeagueAdminRule, LeagueAdminRuleParamType};

use router::Router;

pub mod contract;

mod create_many;
mod update;
mod get_all_by_league;
mod get_all_by_team;
mod get_by_id;
mod create;
mod delete;
mod delete_many;

mod validator;

pub fn init_routes(router: &mut Router) {
    let league_admin_rule = RuleChecker(LeagueAdminRule {
        param_name: "league_id" ,
        param_type: LeagueAdminRuleParamType::LeagueId
    });

    let league_admin_team_event_rule = RuleChecker(LeagueAdminRule {
        param_name: "team_event_id" ,
        param_type: LeagueAdminRuleParamType::TeamEventId
    });

    // Get single
    let handler = ::arounds::HttpCacheHeader{}.around(Box::new(get_by_id::handle));
    router.get("team_events/:team_event_id", handler, "get_team_event");

    // Get all by league_id
    let handler = ::arounds::HttpCacheHeader{}.around(Box::new(get_all_by_league::handle));
    router.get("leagues/:league_id/team_events", handler, "get_all_team_events_by_league");

    // Get all by team_id
    let handler = ::arounds::HttpCacheHeader{}.around(Box::new(get_all_by_team::handle));
    router.get("teams/:team_id/team_events", handler, "get_all_team_events_by_team");



    // Create
    router.post("leagues/:league_id/team_events", league_admin_rule.around(Box::new(create::handle)), "create_team_event");

    // Update
    router.put("team_events/:team_event_id", league_admin_team_event_rule.around(Box::new(update::handle)), "update_team_event");

    // Create many
    router.post("leagues/:league_id/team_events/all", league_admin_rule.around(Box::new(create_many::handle)), "create_team_event_many");

    // Delete
    router.delete("team_events/:team_event_id", league_admin_team_event_rule.around(Box::new(delete::handle)), "delete_team_event");

    // Delete all
    router.delete("leagues/:league_id/team_events/all", league_admin_rule.around(Box::new(delete_many::handle)), "delete_team_event_many");
}

