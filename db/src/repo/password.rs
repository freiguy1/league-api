use diesel::pg::PgConnection;
use diesel::prelude::*;
use schema::user_password;
use error::Error;

#[derive(Queryable, AsChangeset)]
#[table_name="user_password"]
#[changeset_options(treat_none_as_null = "true")]
pub struct PasswordDao {
    pub id: i32,
    pub password: String,
    pub salt: String
}

#[derive(Insertable)]
#[table_name="user_password"]
pub struct NewPassword {
    pub password: String,
    pub salt: String
}

pub struct PasswordRepo<'a> {
    conn: &'a PgConnection
}

impl<'a> PasswordRepo<'a> {
    pub fn new(conn: &'a PgConnection) -> PasswordRepo {
        PasswordRepo {
            conn: conn
        }
    }

    pub fn create(&self, new_password: &NewPassword) -> Result<PasswordDao, Error> {
        let result = try!(::diesel::insert(new_password).into(user_password::table)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn get_single(&self, id: i32) -> Result<Option<PasswordDao>, Error> {
        let result = try!(user_password::table.filter(user_password::id.eq(id))
            .first(self.conn)
            .optional());
        Ok(result)
    }

    pub fn update(&self, updated_password: &PasswordDao) -> Result<PasswordDao, Error> {
        let result = try!(::diesel::update(user_password::table.filter(user_password::id.eq(updated_password.id)))
            .set(updated_password)
            .get_result(self.conn));
        Ok(result)
    }

    pub fn delete(&self, id: i32) -> Result<usize, Error> {
        let result = try!(::diesel::delete(user_password::table.filter(user_password::id.eq(id)))
            .execute(self.conn));
        Ok(result)
    }
}

