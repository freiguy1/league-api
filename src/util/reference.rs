use std::sync::{Arc, Mutex};
use league_db::repo::ReferenceRepo;

#[derive(Serialize, Deserialize, Clone)]
pub struct KeyValue {
    pub key: i32,
    pub value: String
}

impl KeyValue {
    fn new(key: i32, value: String) -> KeyValue {
        KeyValue { key: key, value: value }
    }
}

pub fn sports(cache: &Arc<Mutex<::util::Cache>>, repo: &ReferenceRepo) -> Vec<KeyValue> {
    let mut cache = cache.lock().unwrap();
    if let Some(sports) = cache.get::<Vec<KeyValue>>("sports".to_string()) {
        sports
    } else {
        let sports: Vec<KeyValue> = repo.get_sports()
            .expect("could not retrieve sports")
            .into_iter()
            .map(|sport| KeyValue::new(sport.id, sport.name))
            .collect();
        cache.insert("sports".to_string(), &sports);
        sports
    }

}

pub fn matchup_statuses(cache: &Arc<Mutex<::util::Cache>>, repo: &ReferenceRepo) -> Vec<KeyValue> {
    let mut cache = cache.lock().unwrap();
    if let Some(matchup_statuses) = cache.get::<Vec<KeyValue>>("matchup_statuses".to_string()) {
        matchup_statuses
    } else {
        let matchup_statuses: Vec<KeyValue> = repo.get_matchup_statuses()
            .expect("could not retrieve matchup_statuses")
            .into_iter()
            .map(|matchup_statuses| KeyValue::new(matchup_statuses.id, matchup_statuses.name))
            .collect();
        cache.insert("matchup_statuses".to_string(), &matchup_statuses);
        matchup_statuses
    }

}
