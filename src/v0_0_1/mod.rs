mod team;
mod league;
mod team_event;
mod my;
mod log;

use router::Router;

pub fn init_routes() -> Router {
    let mut router = Router::new();
    team::init_routes(&mut router);
    league::init_routes(&mut router);
    team_event::init_routes(&mut router);
    my::init_routes(&mut router);
    log::init_routes(&mut router);
    router
}
