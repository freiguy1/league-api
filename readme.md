#### League API

The goal for this project is a RESTful API which can be used by any sort of client including a website or mobile app. This will be the second iteration on something like this. I've been part of a team which attempted this in the past, but the project was more or less abandoned. 

At a high level, the API will allow you to create and manage sport leages - mainly targeted at local bar leagues or city rec department leagues. Optimistically this app could also be used to manage higher-profile leagues like little leagues, high school teams and even college and semi pro leagues. Leagues own teams, team events, and game locations (fields, courts, etc). Team events have a date and are either byes or games. Games are comprised of two teams, a time, and a game area. 

In an attempt to keep terminology consistent throughout the development lifetime, I want to get some policies (? this word's a little strong) in place.

**Create** - when you're adding a new item.
**Update** - when you're updating an item.
**Delete** - self explainatory

When accessing all of one thing, the url will look like this: `http://url.com/item-plural/` for instance `http://my-league-api.com/leagues/` will fetch all the leagues.

If this url is followed by a number, then it is fetching one of these items, and the number following the item type in the url specifies its id.  For example `http://my-league-api.com/leagues/1`.

### Rust setup

- To get rust on your machine: [install rustup](https://rustup.rs)
    - If you're on Wandows, you'll have to download the stable installer
- run command `rustup update stable`
- You may have to add rust/cargo to your system PATH so it recognizes commands like `cargo` and `rustc`.  Doing this differs per operating system, but it's an easy google. The directory I added to PATH is `~/.cargo/bin`
- The command `cargo -V` should now give some information
- Install [diesel ORM](http://diesel.rs) with command `cargo install diesel_cli`
- Note: your development machine may also need some development libraries installed. If you're on Wandows, you're on your own, but it's doable.  As of today, these libraries are 
    - libpq-dev
    - libsqlite3-dev
    - libmysqlclient-dev
    - libssl-dev
    - pkg-config
- Check the CI build (`.gitlab-ci.yml`) for any more setup if needed.

### Postgres setup

- log into postgres: `sudo -u postgres psql postgres`
- create dev db user: `create user league_dev_user with password 'league_dev_user_password';`
- create database: `create database league_dev with owner league_dev_user;`
- copy $/.env to $/db/.env. Change db connection string if desired
- in $/db/ folder, run db migrations: `diesel migration run`

### Running the api

- cd to `$/api/` flder.
- depending on how you're configurig the api, you may need to copy $/.env to $/api/.env
- `cargo run` will start the api which is listening on port 6767

### Note

To regen the schema.rs file, change to the db directory and use the command `diesel print-schema > src/schema.rs`. This will only work if you have the environment variable `DATABASE_URL` set up or are using the .env file. This file must reflect the current database, so this command must be ran with any schema changes.
