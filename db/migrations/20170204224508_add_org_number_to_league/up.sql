-- organization
ALTER TABLE league ADD COLUMN organization VARCHAR;

UPDATE league SET organization = 'default org name';

ALTER TABLE league ALTER COLUMN organization SET NOT NULL;


-- contact number
ALTER TABLE league ADD COLUMN contact_phone VARCHAR;

UPDATE league SET contact_phone = '1234567890';

ALTER TABLE league ALTER COLUMN contact_phone SET NOT NULL;
