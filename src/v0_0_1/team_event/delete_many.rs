use iron::prelude::*;
use iron::status;

use league_db::repo::{LeagueRepo, TeamEventRepo};
use std::collections::HashSet;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a u32")));
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let cache = context.cache.clone();

    if itry!(league_repo.get_single(league_id)).is_none() {
        return Ok(Response::with(status::NotFound));
    }

    let existing_team_events = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));

    let team_ids: HashSet<i32> = existing_team_events.iter().flat_map(|g| g.team_ids()).collect();

    for team_id in team_ids.into_iter() {
        ::util::record::invalidate_records_for_team_id(team_id, cache.clone());
    }

    let team_event_ids: Vec<i32> = existing_team_events.iter().map(|g| g.id()).collect();

    itry!(team_event_repo.delete_many(&team_event_ids));

    Ok(Response::with(status::Ok))
}
