use diesel::pg::PgConnection;
use error::Error;
use iron::{Handler, status};
use iron::prelude::*;
use league_db::repo::{OAuth2InfoRepo, OAuth2InfoDao, UserRepo, UserDao};
use router::Router;
use super::contracts::{AuthorizeUrlResponseItem, CreateUserFromOAuth2Response, SuccessfulOAuth2Response};

mod google;

pub fn init_routes(router: &mut Router, config: &::context::Config) {
    let mut authorize_urls = Vec::new();
    // Google callback
    if let Some(ref google_auth) = config.google_auth {
        let google_strategy = google::GoogleOAuth2Strategy::new(
            &google_auth.client_id,
            &google_auth.client_secret,
            &config.base_url);

        authorize_urls.push(AuthorizeUrlResponseItem {
            provider: google::NAME,
            url: google_strategy.authorize_url()
        });

        let google_handler = OAuth2Callback {
            strategy: google_strategy,
            name: google::NAME
        };

        router.get(google::CALLBACK_PATH, google_handler, "google_oauth2_callback");
    }

    // Facebook callback goes here

    let authorize_urls_handler = AuthorizeUrls {
        items: authorize_urls
    };
    router.get("/auth/oauth", authorize_urls_handler, "authorize_urls");
}

struct AuthorizeUrls {
    items: Vec<AuthorizeUrlResponseItem>
}

impl Handler for AuthorizeUrls {
    fn handle(&self, _: &mut Request) -> IronResult<Response> {
        let response_body = itry!(::serde_json::to_string(&self.items));
        Ok(::util::json_response(response_body, status::Ok))
    }
}

struct OAuth2ProfileInfo {
    id: String,
    email: String,
    first_name: String,
    last_name: String,
    phone: Option<String>
}

trait OAuth2Strategy: Send + Sync + ::std::any::Any + 'static{
    fn get_token(&self, req: &mut Request) -> Result<String, Error>;
    fn get_profile_info(&self, token: String) -> Result<OAuth2ProfileInfo, Error>;
    fn authorize_url(&self) -> String;
}

struct OAuth2Callback<OS: OAuth2Strategy> {
    strategy: OS,
    name: &'static str
}

impl<OS: OAuth2Strategy> OAuth2Callback<OS> {
    fn get_oauth2_info(&self, conn: &PgConnection, profile_info: &OAuth2ProfileInfo) -> Result<Option<OAuth2InfoDao>, Error> {
        let oauth2_info_repo = OAuth2InfoRepo::new(&conn);
        let row = oauth2_info_repo.get_by_provider_profile_id(self.name, &profile_info.id)?;
        Ok(row)
    }

    fn get_user(&self, conn: &PgConnection, profile_info: &OAuth2ProfileInfo) -> Result<Option<UserDao>, Error> {
        let user_repo = UserRepo::new(&conn);
        let row = user_repo.get_by_email(&profile_info.email)?;
        Ok(row)
    }

    fn create_oauth2_info(&self, conn: &PgConnection, profile_info: &OAuth2ProfileInfo, user_id: Option<i32>) -> Result<i32, Error> {
        let oauth2_info_repo = OAuth2InfoRepo::new(&conn);
        let result = oauth2_info_repo.create(self.name, &profile_info.id, &profile_info.email, user_id)?;
        Ok(result.id)
    }

    fn create_user_response(profile_info: OAuth2ProfileInfo, oauth2_info_id: i32) -> IronResult<Response> {
        let response_obj = CreateUserFromOAuth2Response {
            email: profile_info.email,
            first_name: profile_info.first_name,
            last_name: profile_info.last_name,
            phone: profile_info.phone,
            oauth2_info_id: oauth2_info_id
        };
        let response_body = itry!(::serde_json::to_string(&response_obj));
        let response = ::util::json_response(response_body, status::Ok);
        Ok(response)
    }

    fn create_successful_response(token: String) -> IronResult<Response> {
        let response_obj = SuccessfulOAuth2Response { token: token.clone() };
        let response_body = itry!(::serde_json::to_string(&response_obj));
        let mut response = ::util::json_response(response_body, status::Ok);
        super::add_token_to_response(&mut response, token);
        Ok(response)
    }

}

impl<OS: OAuth2Strategy> Handler for OAuth2Callback<OS> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let token = itry!(self.strategy.get_token(req));
        let profile_info = itry!(self.strategy.get_profile_info(token));

        let context = req.extensions.get::<::context::ApiContext>().unwrap();
        let ref conn = itry!(context.pool.get());
        if let Some(oauth2_info) = itry!(self.get_oauth2_info(&conn, &profile_info)) {
            if let Some(user_id) = oauth2_info.user_id {
                // OAuth2Info exists with user id
                let token = super::create_token(user_id, &context.config);
                Self::create_successful_response(token)
            } else {
                // OAuth2Info exists with no user id (not that common)
                Self::create_user_response(profile_info, oauth2_info.id)
            }
        } else if let Some(user) = itry!(self.get_user(&conn, &profile_info)) {
            // User exists by email but not oauth2info
            itry!(self.create_oauth2_info(&conn, &profile_info, Some(user.id)));
            let token = super::create_token(user.id, &context.config);
            Self::create_successful_response(token)
        } else {
            // User doesn't exist at all
            let oauth2_info_id = itry!(self.create_oauth2_info(&conn, &profile_info, None));
            Self::create_user_response(profile_info, oauth2_info_id)
        }
    }
}
