use iron::prelude::*;
use iron::status;

use super::contract::{CreateLeagueRequest, CreateLeagueResponse};
use league_db::repo::{ReferenceRepo, LeagueRepo, NewLeague, NewMatchupLocation, NewAdministrator};

use std::collections::HashSet;

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let mut create_league_request = match ::util::parse_body::<CreateLeagueRequest>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let repo = LeagueRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let sports = ::util::reference::sports(&context.cache, &reference_repo);

    let validation_errors = super::validator::validate(&mut create_league_request, &sports, &HashSet::new(), None, &vec![]);
    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    let league_dao = build_league_dao(&create_league_request, &sports, context.user_id.unwrap());

    let created_league_id = itry!(repo.create(&league_dao));

    let create_league_response = CreateLeagueResponse {
        id: created_league_id
    };

    match context.emailer() {
        Some(e) => {
            e.send_async(
                "ethan.frei@gmail.com",
                "Master Mind",
                "A new league was created",
                &format!("League '{}' created with id: {:?}", league_dao.name, created_league_id));
        },
        None => {}
    }

    // Return response
    let response_body = itry!(::serde_json::to_string(&create_league_response));
    let response = ::util::json_response(response_body, status::Ok);
    Ok(response)
}

fn build_league_dao(req: &CreateLeagueRequest, sports: &Vec<::util::reference::KeyValue>, admin_user_id: i32) -> NewLeague {
    let sport_id = sports
        .iter()
        .filter(|s| s.value.to_lowercase() == req.sport.to_lowercase())
        .next()
        .unwrap().key;
    NewLeague {
        name: req.name.clone(),
        information: req.information.clone(),
        sport_id: sport_id,
        organization: req.organization.clone(),
        contact_phone: req.contact_phone.clone(),
        matchup_locations: req.matchup_locations.iter().map(|loc| NewMatchupLocation {
            name: loc.clone()
        }).collect(),
        administrators: vec![NewAdministrator {
            user_id: admin_user_id
        }],
        games_per_matchup: req.games_per_matchup as i32
    }
}

