use chrono::{NaiveDate, UTC, DateTime};

#[derive(Debug, Clone, Serialize)]
pub struct GetTeamsResponseItem {
    pub sequence: i32,
    pub id: i32,
    pub league_name: String,
    pub league_id: i32,
    pub name: String,
    pub wins: i32,
    pub losses: i32,
    pub ties: i32,
    pub remaining_team_events: i32,
    pub sport: String,
    pub next_team_events: Vec<NextTeamEvent>
}

#[derive(Debug, Clone, Serialize)]
pub struct NextTeamEvent {
    // Shared
    pub id: i32,
    pub notes: Option<String>,
    // Matchup
    pub datetime: Option<DateTime<UTC>>,
    pub location: Option<String>,
    pub is_home: Option<bool>,
    pub against_team_name: Option<String>,
    // Bye
    pub bye_date: Option<NaiveDate>
}
