#[macro_use(itry)] extern crate iron;
extern crate bodyparser;
extern crate persistent;
extern crate router;
extern crate mount;
extern crate urlencoded;
extern crate url;
extern crate regex;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate chrono;
extern crate league_db;
extern crate diesel;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate pwhash;
extern crate rand;
extern crate jwt;
extern crate crypto;
extern crate hyper;
extern crate hyper_native_tls;
extern crate lettre;
extern crate uuid;
extern crate oauth2;

use iron::prelude::*;
use router::Router;
use mount::Mount;

mod v0_0_1;
mod error;
mod arounds;
mod auth;
mod stats;
mod context;
mod util;

fn main() {

    // Init router
    let mut mount = Mount::new();
    let mut router = Router::new();
    let context_manager = context::ContextManager::new();

    auth::init_routes(&mut router, &context_manager.config);
    mount.mount("/", router);

    let v0_0_1_router = v0_0_1::init_routes();
    mount.mount("/v0.0.1/", v0_0_1_router);

    let mut chain = Chain::new(mount);

    let port = context_manager.config.port.clone();
    chain.link_before(context_manager);

    chain.link_after(arounds::ErrorPrint);
    chain.link_after(auth::TokenRefresh);


    Iron::new(chain).http(("0.0.0.0", port)).expect("Unable to start Iron server");
}
