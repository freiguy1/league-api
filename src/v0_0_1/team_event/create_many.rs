use chrono::NaiveTime;
use iron::prelude::*;
use iron::status;

use super::contract::CreateTeamEventsRequestItem;
use league_db::repo::{NewMatchup, NewBye, NewGame, TeamEventRepo, LeagueRepo, ReferenceRepo, TeamRepo};

pub fn handle(req: &mut Request) -> IronResult<Response> {
    let league_id = if let Some(league_id) = ::util::get_i32_from_request(&req, "league_id") {
        league_id
    } else {
        return Ok(Response::with((status::BadRequest, "league_id in url should be a u32")));
    };

    let create_team_events_request = match ::util::parse_body::<Vec<CreateTeamEventsRequestItem>>(req) {
        Ok(parsed) => parsed,
        Err(resp) => return Ok(resp)
    };

    let context = req.extensions.get::<::context::ApiContext>().unwrap();
    let ref conn = itry!(context.pool.get());
    let team_event_repo = TeamEventRepo::new(&conn);
    let reference_repo = ReferenceRepo::new(&conn);
    let league_repo = LeagueRepo::new(&conn);
    let team_repo = TeamRepo::new(&conn);

    let league = match itry!(league_repo.get_single(league_id)) {
        Some(l) => l,
        None => return Ok(Response::with(status::NotFound))
    };

    let statuses = ::util::reference::matchup_statuses(&context.cache, &reference_repo);
    let teams = itry!(team_repo.get_by_league_id(league_id));
    let existing_team_events = itry!(team_event_repo.get_by_league_ids(&vec![league_id]));

    if existing_team_events.len() > 0 {
        return Ok(Response::with((status::BadRequest, "Cannot create team events if some already exist")));
    }

    let validation_errors = validate(&create_team_events_request,
                                     &statuses,
                                     teams.iter().map(|t| t.id).collect(),
                                     league.matchup_locations.iter().map(|l| l.id).collect());

    if !validation_errors.is_empty() {
        let response_body = itry!(::serde_json::to_string(&validation_errors));
        return Ok(::util::json_response(response_body, status::BadRequest));
    }

    for create_team_events_request_item in create_team_events_request.into_iter() {
        if create_team_events_request_item.home_team_id.is_some() {
            let dao = build_new_matchup_dao(league_id, create_team_events_request_item, &statuses);
            itry!(team_event_repo.create_matchup(&dao));
        } else {
            let dao = build_new_bye_dao(league_id, create_team_events_request_item);
            itry!(team_event_repo.create_bye(&dao));
        };
    }

    Ok(Response::with(status::Ok))
}

fn build_new_matchup_dao(league_id: i32, req: CreateTeamEventsRequestItem, statuses: &Vec<::util::reference::KeyValue>) -> NewMatchup {
    let games: Vec<NewGame> = if let Some(ref games) = req.games { 
        games.iter().map(|g| NewGame {
            sequence: g.sequence,
            home_team_score: g.home_team_score,
            away_team_score: g.away_team_score,
            notes: g.notes.clone()
        }).collect()
    } else {
        Vec::new()
    };

    NewMatchup {
        datetime: req.datetime.unwrap().naive_utc(),
        matchup_location_id: req.location_id.unwrap(),
        league_id: league_id,
        home_team_id: req.home_team_id.unwrap(),
        away_team_id: req.away_team_id.unwrap(),
        matchup_status_id: statuses.iter().filter(|s| s.value.to_lowercase() == req.status.as_ref().unwrap().to_lowercase()).next().unwrap().key,
        rescheduled_team_event_id: None,
        notes: req.notes.clone(),
        games: games
    }
}

fn build_new_bye_dao(league_id: i32, req: CreateTeamEventsRequestItem) -> NewBye {
    NewBye {
        datetime: req.bye_date.unwrap().and_time(NaiveTime::from_hms(0, 0, 0)),
        league_id: league_id,
        team_id: req.bye_team_id.unwrap(),
        notes: req.notes.clone()
    }
}

fn validate(req: &Vec<CreateTeamEventsRequestItem>,
            statuses: &Vec<::util::reference::KeyValue>,
            team_ids: Vec<i32>,
            location_ids: Vec<i32>) -> Vec<String> {
    req.iter().flat_map(|i| super::validator::validate(
        i,
        statuses,
        &team_ids,
        &location_ids,
        &vec![], // no existing team events
        None)).collect()
}

