use diesel::result::Error as DieselError;
use std::error::Error as StdError;

#[derive(Debug)]
pub enum Error {
    // Diesel(Box<DieselError>),
    LeagueDb(String)
}

impl From<DieselError> for Error {
    fn from(e: DieselError) -> Error {
        // Error::Diesel(Box::new(e))
        Error::LeagueDb(e.description().to_string())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match self {
            // &Error::Diesel(ref e) => e.description(),
            &Error::LeagueDb(ref message) => &message
        }
    }

    fn cause(&self) -> Option<&StdError> {
        match *self {
            // Error::Diesel(ref e) => Some(e),
            Error::LeagueDb(_) => None
        }
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        f.write_str(self.description())
    }
}
